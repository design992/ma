package io.github.jhipster.sample.domain;



import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

/**
 * Klasa: CompanyInfo.
 */
@Entity
@Table(name = "jhi_company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class Company extends AbstractAuditingEntity implements Serializable {

    /** Stala serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /** contact person. */
    @ManyToOne
    @JoinColumn(name = "contact_person_id")
    private User contactPerson;

    /** name. */
    @Size(min = 1, max = 50)
    @Column(name = "name", length = 50)
    private String name;

    /** website. */
    @Size(min = 0, max = 50)
    @Column(name = "website", length = 50)
    private String website;

    /** phone. */
    @Size(min = 0, max = 20)
    @Column(name = "phone", length = 20)
    private String phone;

    /** users. */
    @OneToMany(mappedBy = "company")
    private Set<User> users;

    /** version. */
    @Version
    @Column(name = "version", nullable = false)
    private long version;

    public Company() {
        super();
    }

    public Long getId() {
        return id;
    }

    /**
     * Ustawia wartosc pola id.
     *
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    public User getContactPerson() {
        return contactPerson;
    }

    /**
     * Ustawia wartosc pola contact person.
     *
     * @param contactPerson
     *            the contactPerson to set
     */
    public void setContactPerson(User contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * Zwraca wartosc pola name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Ustawia wartosc pola name.
     *
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Zwraca wartosc pola website.
     *
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Ustawia wartosc pola website.
     *
     * @param website
     *            the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * Zwraca wartosc pola phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Ustawia wartosc pola phone.
     *
     * @param phone
     *            the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Zwraca wartosc pola billing infos.
     *
     * @return the billingInfos
     */
//
//    public Set<User> getUsers() {
//        return users;
//    }
//
//    public void setUsers(Set<User> users) {
//        this.users = users;
//    }

    /**
     * Zwraca wartosc pola teams.
     *
     * @return the teams
     */
    public long getVersion() {
        return version;
    }

    /**
     * Ustawia wartosc pola version.
     *
     * @param version
     *            the version to set
     */
    public void setVersion(long version) {
        this.version = version;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((website == null) ? 0 : website.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Company other = (Company) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (phone == null) {
            if (other.phone != null) {
                return false;
            }
        } else if (!phone.equals(other.phone)) {
            return false;
        }
        if (website == null) {
            if (other.website != null) {
                return false;
            }
        } else if (!website.equals(other.website)) {
            return false;
        }
        return true;
    }

}
