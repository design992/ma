package io.github.jhipster.sample.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "jhi_configuration")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Configuration extends AbstractAuditingEntity implements Serializable {

    public static final String FEEDBACK_RECEIVER_EMAIL = "feedbackReceiverEmail";


    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @NotNull
    private String featureName;

    @NotNull
    private String featureValue;

    @Enumerated(EnumType.STRING)
    private DataType type;

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getFeatureValue() {
        return featureValue;
    }

    public void setFeatureValue(String featureValue) {
        this.featureValue = featureValue;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((featureName == null) ? 0 : featureName.hashCode());
        result = prime * result + ((featureValue == null) ? 0 : featureValue.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Configuration other = (Configuration) obj;
        if (featureName == null) {
            if (other.featureName != null) {
                return false;
            }
        } else if (!featureName.equals(other.featureName)) {
            return false;
        }
        if (featureValue == null) {
            if (other.featureValue != null) {
                return false;
            }
        } else if (!featureValue.equals(other.featureValue)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Configuration [code=" + code + ", featureName=" + featureName + ", featureValue=" + featureValue
            + ", type=" + type + "]";
    }

}
