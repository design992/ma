package io.github.jhipster.sample.domain;

public enum DataType {
    STRING, LONG, DOUBLE, BOOLEAN, DATE, INTEGER
}
