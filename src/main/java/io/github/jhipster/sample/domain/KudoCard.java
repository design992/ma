package io.github.jhipster.sample.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by PK on 2017-09-03.
 */
@Entity
@Table(name = "kudo_card")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KudoCard extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 3)
    @Column(name = "content", nullable = false)
    private String content;

    @ManyToOne
    @JoinColumn(name = "to_user")
    private User toUser;

    @ManyToOne
    @JoinColumn(name = "from_user")
    private User fromUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KudoCard kudoCard = (KudoCard) o;

        if (id != null ? !id.equals(kudoCard.id) : kudoCard.id != null) return false;
        if (content != null ? !content.equals(kudoCard.content) : kudoCard.content != null) return false;
        if (toUser != null ? !toUser.equals(kudoCard.toUser) : kudoCard.toUser != null) return false;
        return fromUser != null ? fromUser.equals(kudoCard.fromUser) : kudoCard.fromUser == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (toUser != null ? toUser.hashCode() : 0);
        result = 31 * result + (fromUser != null ? fromUser.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "KudoCard{" +
            "id=" + id +
            ", content='" + content + '\'' +
            ", toUser=" + toUser +
            ", fromUser=" + fromUser +
            '}';
    }
}
