package io.github.jhipster.sample.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;


@Entity
@Table(name = "T_TASK")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class Task extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column(name = "task_status")
    private TaskStatus taskStatus;

    @Column(name = "task_priority")
    private TaskPriority taskPriority;

    @ManyToOne
    @JoinColumn(name = "creator")
//    @JsonIgnore
    private User creator;

    @ManyToOne
    @JoinColumn(name = "executor")
//    @JsonIgnore
    private User executor;

    @Column(name = "creatorid")
    private Long creatorId;

    @Column(name = "executorid")
    private Long executorId;

    @Column(name = "begin_date")
//    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private Instant beginDate;


    @Column(name = "end_date")
//    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private Instant endDate;

    private Double spendedTime;

    @OneToMany(mappedBy = "task")
//    @JoinTable(name = "jhi_TASK_COMMENT", joinColumns = @JoinColumn(name = "task_id") , inverseJoinColumns = @JoinColumn(name = "post_id") )
    private List<Comment> posts;

    private String status;

    private String priority;

    @OneToMany(mappedBy = "task")
//    @Column(name = "taskEstimations")
    private List<TaskEstimation> taskEstimations;

    @OneToMany(mappedBy = "task")
    private List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser;
    @Transient
    private String comment;
    @Transient
    private Double estimation;
    @Transient
    private Double totalSpendedTime;

    @Transient
    private Double averageEstimations;
    private Boolean overtimed;
//    @Version
//    private long version;

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TaskPriority getTaskPriority() {
        return taskPriority;
    }

    public Double getTotalSpendedTime() {
        return totalSpendedTime;
    }

    public void setTotalSpendedTime(Double totalSpendedTime) {
        this.totalSpendedTime = totalSpendedTime;
    }

    public void setTaskPriority(TaskPriority taskPriority) {
        this.taskPriority = taskPriority;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getExecutor() {
        return executor;
    }

    public void setExecutor(User executor) {
        this.executor = executor;
    }

    public Long getExecutorId() {
        return executorId;
    }

    public void setExecutorId(Long executorId) {
        this.executorId = executorId;
    }

    public Instant getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Instant beginDate) {
        this.beginDate = beginDate;
    }

    public Instant Instant() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Instant getEndDate() {
        return endDate;
    }
    public Double getSpendedTime() {
        return spendedTime;
    }

    public void setSpendedTime(Double spendedTime) {
        this.spendedTime = spendedTime;
    }

    public List<Comment> getPosts() {
        return posts;
    }

    public void setPosts(List<Comment> posts) {
        this.posts = posts;
    }

    public List<TaskEstimation> getTaskEstimations() {
        return taskEstimations;
    }

    public void setTaskEstimations(List<TaskEstimation> taskEstimations) {
        this.taskEstimations = taskEstimations;
    }

    public Boolean getOvertimed() {
        return overtimed;
    }

    public void setOvertimed(Boolean overtimed) {
        this.overtimed = overtimed;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
//    public long getVersion() {
//        return version;
//    }
//
//    public void setVersion(long version) {
//        this.version = version;
//    }

    public List<TimeSpendedOnTaskByUser> getTimeSpendedOnTaskByUser() {
        return timeSpendedOnTaskByUser;
    }

    public void setTimeSpendedOnTaskByUser(List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser) {
        this.timeSpendedOnTaskByUser = timeSpendedOnTaskByUser;
    }

    public Double getEstimation() {
        return estimation;
    }

    public void setEstimation(Double estimation) {
        this.estimation = estimation;
    }

    public Double getAverageEstimations() {
        return averageEstimations;
    }

    public void setAverageEstimations(Double averageEstimations) {
        this.averageEstimations = averageEstimations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != null ? !id.equals(task.id) : task.id != null) return false;
        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        if (description != null ? !description.equals(task.description) : task.description != null) return false;
        if (taskStatus != null ? !taskStatus.equals(task.taskStatus) : task.taskStatus != null) return false;
        if (taskPriority != null ? !taskPriority.equals(task.taskPriority) : task.taskPriority != null) return false;
        if (creator != null ? !creator.equals(task.creator) : task.creator != null) return false;
        if (executor != null ? !executor.equals(task.executor) : task.executor != null) return false;
        if (beginDate != null ? !beginDate.equals(task.beginDate) : task.beginDate != null) return false;
        if (endDate != null ? !endDate.equals(task.endDate) : task.endDate != null) return false;
        if (status != null ? !status.equals(task.status) : task.status != null) return false;
        return priority != null ? priority.equals(task.priority) : task.priority == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (taskStatus != null ? taskStatus.hashCode() : 0);
        result = 31 * result + (taskPriority != null ? taskPriority.hashCode() : 0);
        result = 31 * result + (creator != null ? creator.hashCode() : 0);
        result = 31 * result + (executor != null ? executor.hashCode() : 0);
        result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", taskStatus=" + taskStatus +
                ", taskPriority=" + taskPriority +
                ", creator=" + creator +
                ", executor=" + executor +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", status='" + status + '\'' +
                ", priority='" + priority + '\'' +
                '}';
    }
}


