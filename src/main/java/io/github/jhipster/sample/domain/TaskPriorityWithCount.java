package io.github.jhipster.sample.domain;

/**
 * Created by pkarwowski on 23.07.2017.
 */
public class TaskPriorityWithCount {
    private TaskPriority taskPriority;
    private Integer count;

    public TaskPriority getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(TaskPriority taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskPriorityWithCount that = (TaskPriorityWithCount) o;

        if (taskPriority != null ? !taskPriority.equals(that.taskPriority) : that.taskPriority != null) return false;
        return count != null ? count.equals(that.count) : that.count == null;
    }

    @Override
    public int hashCode() {
        int result = taskPriority != null ? taskPriority.hashCode() : 0;
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaskPriorityWithCount{" +
            "taskPriority=" + taskPriority +
            ", count=" + count +
            '}';
    }
}
