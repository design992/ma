package io.github.jhipster.sample.domain;

/**
 * Created by pkarwowski on 23.07.2017.
 */
public class TaskStatusWithCount {
    private TaskStatus taskStatus;
    private Integer count;

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskStatusWithCount that = (TaskStatusWithCount) o;

        if (taskStatus != null ? !taskStatus.equals(that.taskStatus) : that.taskStatus != null) return false;
        return count != null ? count.equals(that.count) : that.count == null;
    }

    @Override
    public int hashCode() {
        int result = taskStatus != null ? taskStatus.hashCode() : 0;
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaskStatusWithCount{" +
            "taskStatus=" + taskStatus +
            ", count=" + count +
            '}';
    }
}
