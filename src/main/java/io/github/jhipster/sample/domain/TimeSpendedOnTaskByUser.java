package io.github.jhipster.sample.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by PK on 2017-03-03.
 */


@Entity
@Table(name = "jhi_time_spended")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class TimeSpendedOnTaskByUser extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;

    private Double timeEstimatedByUser;



    public TimeSpendedOnTaskByUser() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Double getTimeEstimatedByUser() {
        return timeEstimatedByUser;
    }

    public void setTimeEstimatedByUser(Double timeEstimatedByUser) {
        this.timeEstimatedByUser = timeEstimatedByUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimeSpendedOnTaskByUser that = (TimeSpendedOnTaskByUser) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (task != null ? !task.equals(that.task) : that.task != null) return false;
        return timeEstimatedByUser != null ? timeEstimatedByUser.equals(that.timeEstimatedByUser) : that.timeEstimatedByUser == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (task != null ? task.hashCode() : 0);
        result = 31 * result + (timeEstimatedByUser != null ? timeEstimatedByUser.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TimeSpendedOnTaskByUser{" +
                "id=" + id +
                ", user=" + user +
                ", task=" + task +
                ", timeEstimatedByUser=" + timeEstimatedByUser +
                '}';
    }
}
