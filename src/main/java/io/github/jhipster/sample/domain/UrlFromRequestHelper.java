package io.github.jhipster.sample.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class UrlFromRequestHelper {

    private static final Logger log = LoggerFactory.getLogger(UrlFromRequestHelper.class);

    public String prepareAppURLBasedOnRequest(HttpServletRequest request) {
        log.debug("on enter: prepareAppURLBasedOnRequest");
        StringBuilder requestUrl = new StringBuilder();
        requestUrl.append(request.getScheme()).append("://").append(request.getServerName()).append(":")
            .append(request.getServerPort()).append(request.getContextPath());

        return requestUrl.toString();
    }

}
