package io.github.jhipster.sample.domain;

import com.fasterxml.jackson.annotation.*;
import io.github.jhipster.sample.config.Constants;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * A user.
 */
@Entity
@Table(name = "jhi_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class User extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;
    @JsonIgnore
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60)
    private String password;
    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;
    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;
    @Email
    @Size(min = 5, max = 100)
    @Column(length = 100, unique = true)
    private String email;
    @Column(nullable = false)
    private boolean activated = false;
    @Size(min = 2, max = 5)
    @Column(name = "lang_key", length = 5)
    private String langKey;
    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;
    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    private String resetKey;

    @Column(name = "reset_date")
    private Instant resetDate = null;

    @Size(max = 400)
    @Column(length = 400, name = "next_project")
    private String nextProject;

    @Size(max = 400)
    @Column(length = 400, name = "motivation")
    private String motivation;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "jhi_user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<Authority> authorities = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PersistentToken> persistentTokens = new HashSet<>();

    //    @NotNull
    @ManyToOne
    private Company company;

    @Version
    @Column(name = "version")
    private long version;

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private List<Comment> posts;

    @OneToMany(mappedBy = "creator")
    private List<Task> tasks;

    @OneToMany(mappedBy = "executor")
    private List<Task> tasksExecuted;

    @OneToMany(mappedBy = "fromUser")
    @JsonIgnoreProperties("fromUser")
    private List<KudoCard> kudoCardsSent;

    @OneToMany(mappedBy = "toUser")
    @JsonIgnoreProperties("toUser")
    private List<KudoCard> kudoCardsReceived;


    @OneToMany(mappedBy = "user")
    private List<TaskEstimation> taskEstimations;
    private String certificates;

    @OneToMany(mappedBy = "user")
    private List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser;
    @OneToMany(mappedBy = "user")
    private Set<Skill> skills = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    //Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = login.toLowerCase(Locale.ENGLISH);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Set<PersistentToken> getPersistentTokens() {
        return persistentTokens;
    }

    public void setPersistentTokens(Set<PersistentToken> persistentTokens) {
        this.persistentTokens = persistentTokens;
    }

    public String getNextProject() {
        return nextProject;
    }

    public void setNextProject(String nextProject) {
        this.nextProject = nextProject;
    }

    public boolean isActivated() {
        return activated;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public List<Comment> getPosts() {
        return posts;
    }

    public void setPosts(List<Comment> posts) {
        this.posts = posts;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Task> getTasksExecuted() {
        return tasksExecuted;
    }

    public void setTasksExecuted(List<Task> tasksExecuted) {
        this.tasksExecuted = tasksExecuted;
    }

    public List<TaskEstimation> getTaskEstimations() {
        return taskEstimations;
    }

    public void setTaskEstimations(List<TaskEstimation> taskEstimations) {
        this.taskEstimations = taskEstimations;
    }

    public List<TimeSpendedOnTaskByUser> getTimeSpendedOnTaskByUser() {
        return timeSpendedOnTaskByUser;
    }

    public void setTimeSpendedOnTaskByUser(List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser) {
        this.timeSpendedOnTaskByUser = timeSpendedOnTaskByUser;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public String getMotivation() {
        return this.motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    public List<KudoCard> getKudoCardsSent() {
        return kudoCardsSent;
    }

    public void setKudoCardsSent(List<KudoCard> kudoCardsSent) {
        this.kudoCardsSent = kudoCardsSent;
    }

    public List<KudoCard> getKudoCardsReceived() {
        return kudoCardsReceived;
    }

    public void setKudoCardsReceived(List<KudoCard> kudoCardsReceived) {
        this.kudoCardsReceived = kudoCardsReceived;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        return login.equals(user.login);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (activated ? 1 : 0);
        result = 31 * result + (langKey != null ? langKey.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (activationKey != null ? activationKey.hashCode() : 0);
        result = 31 * result + (resetKey != null ? resetKey.hashCode() : 0);
        result = 31 * result + (resetDate != null ? resetDate.hashCode() : 0);
        result = 31 * result + (nextProject != null ? nextProject.hashCode() : 0);
        result = 31 * result + (motivation != null ? motivation.hashCode() : 0);
        result = 31 * result + (authorities != null ? authorities.hashCode() : 0);
        result = 31 * result + (persistentTokens != null ? persistentTokens.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (posts != null ? posts.hashCode() : 0);
        result = 31 * result + (tasks != null ? tasks.hashCode() : 0);
        result = 31 * result + (tasksExecuted != null ? tasksExecuted.hashCode() : 0);
        result = 31 * result + (taskEstimations != null ? taskEstimations.hashCode() : 0);
        result = 31 * result + (timeSpendedOnTaskByUser != null ? timeSpendedOnTaskByUser.hashCode() : 0);
        result = 31 * result + (skills != null ? skills.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated='" + activated + '\'' +
            ", langKey='" + langKey + '\'' +
            ", activationKey='" + activationKey + '\'' +
            "}";
    }
}
