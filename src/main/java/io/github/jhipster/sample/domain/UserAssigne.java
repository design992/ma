package io.github.jhipster.sample.domain;

import io.github.jhipster.sample.service.dto.SimpleUserDTO;

/**
 * Created by PK on 2017-08-12.
 */
public class UserAssigne {
    private SimpleUserDTO userDTO;
    private int count;

    public SimpleUserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(SimpleUserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
