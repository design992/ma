package io.github.jhipster.sample.domain;

import io.github.jhipster.sample.service.dto.SimpleUserDTO;
import io.github.jhipster.sample.service.dto.SkillDTO;

import java.util.List;

/**
 * Created by PK on 2017-08-14.
 */
public class UserSkill {
    private SimpleUserDTO simpleUserDTO;
    private List<SkillDTO> skill;

    public SimpleUserDTO getSimpleUserDTO() {
        return simpleUserDTO;
    }

    public void setSimpleUserDTO(SimpleUserDTO simpleUserDTO) {
        this.simpleUserDTO = simpleUserDTO;
    }

    public List<SkillDTO> getSkill() {
        return skill;
    }

    public void setSkill(List<SkillDTO> skill) {
        this.skill = skill;
    }
}
