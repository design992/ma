package io.github.jhipster.sample.domain;

/**
 * Created by pkarwowski on 13.07.2017.
 */
public enum UserState {
    INVITED, ACTIVE, DEACTIVATED;
}
