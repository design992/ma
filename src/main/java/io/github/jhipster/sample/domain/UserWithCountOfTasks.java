package io.github.jhipster.sample.domain;

import io.github.jhipster.sample.service.dto.SimpleUserDTO;

/**
 * Created by pkarwowski on 12.07.2017.
 */
public class UserWithCountOfTasks {
    private SimpleUserDTO simpleUserDTO;
    private Integer kount;

    public SimpleUserDTO getSimpleUserDTO() {
        return simpleUserDTO;
    }

    public void setSimpleUserDTO(SimpleUserDTO simpleUserDTO) {
        this.simpleUserDTO = simpleUserDTO;
    }

    public Integer getKount() {
        return kount;
    }

    public void setKount(Integer kount) {
        this.kount = kount;
    }

    public UserWithCountOfTasks(SimpleUserDTO simpleUserDTO, Integer kount) {
        this.simpleUserDTO = simpleUserDTO;
        this.kount = kount;
    }

    public UserWithCountOfTasks() {
    }

    @Override
    public String toString() {
        return "UserWithCountOfTasks{" +
            "simpleUserDTO=" + simpleUserDTO +
            ", kount=" + kount +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserWithCountOfTasks that = (UserWithCountOfTasks) o;

        if (simpleUserDTO != null ? !simpleUserDTO.equals(that.simpleUserDTO) : that.simpleUserDTO != null)
            return false;
        return kount != null ? kount.equals(that.kount) : that.kount == null;
    }

    @Override
    public int hashCode() {
        int result = simpleUserDTO != null ? simpleUserDTO.hashCode() : 0;
        result = 31 * result + (kount != null ? kount.hashCode() : 0);
        return result;
    }
}

