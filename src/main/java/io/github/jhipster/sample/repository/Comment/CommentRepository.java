package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.Comment;
import io.github.jhipster.sample.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Created by PK on 2017-03-04.
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {

//    @Query("SELECT distinct t FROM Comment t LEFT OUTER JOIN FETCH t.user u LEFT OUTER JOIN FETCH t.company c where c.id = :id ")
//    Set<Comment> findAllByCompanyId(@Param("id") Long id);

    @Query("SELECT s FROM Comment s LEFT OUTER JOIN FETCH s.user u WHERE u.id = :id")
    List<Comment> findAllByUserId(@Param("id") long id);

    @Query("SELECT s FROM Comment s LEFT OUTER JOIN FETCH s.user u WHERE u.login = :login")
    List<Comment> findAllByUserLogin(@Param("login") String login);

    @Query("SELECT s FROM Comment s LEFT OUTER JOIN FETCH s.user u WHERE u = :user")
    List<Comment> findAllByUser(@Param("user") User user);

}
