/*
 *
 */
package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query("FROM Company c LEFT JOIN FETCH c.users user WHERE user.id = (:id)")
    Optional<Company> findOneByUserId(@Param("id") Long id);

//    @Query("FROM Company c LEFT OUTER JOIN FETCH c.billingInfos bi LEFT OUTER JOIN FETCH c.plan p")
//    List<Company> findAllCompanies();
}
