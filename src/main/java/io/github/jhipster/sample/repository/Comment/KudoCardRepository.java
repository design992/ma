package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.KudoCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by PK on 2017-09-03.
 */
//@Repository
public interface KudoCardRepository extends JpaRepository<KudoCard, Long> {
    @Query("SELECT card FROM KudoCard card WHERE card.fromUser.id = :id")
    List<KudoCard> findAllByFromUserId(@Param("id") Long id);

    @Query("SELECT card FROM KudoCard card WHERE card.toUser.id = :id")
    List<KudoCard> findAllByToUserId(@Param("id") Long id);
}
