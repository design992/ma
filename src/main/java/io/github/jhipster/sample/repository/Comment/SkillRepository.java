package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.Skill;
import io.github.jhipster.sample.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PK on 2017-03-08.
 */
@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
    Skill findByName(String name);
    List<Skill> findByUser(User user);
}

