package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.TaskPriority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by PK on 2017-03-08.
 */
public interface TaskPriorityRepository extends JpaRepository<TaskPriority, Long> {
    TaskPriority findByName(String name);
}

