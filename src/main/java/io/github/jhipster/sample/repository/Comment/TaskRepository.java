package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by PK on 2017-03-08.
 */
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT t FROM Task t LEFT OUTER JOIN FETCH t.creator u LEFT OUTER JOIN FETCH t.executor e WHERE u.id = :id or e.id = :id")
    List<Task> findAllByUserId(@Param("id") Long id);

//    @Query("SELECT t FROM Task t LEFT OUTER JOIN FETCH t.creator u WHERE u.name = :name")
//    List<Task> findAllByUserName(@Param("name") String name);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.status = 'OPENED')")
    List<Task> findAllByCreatorIdAndTaskStatusOpened(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.status.name = 'CREATED')")
    List<Task> findAllByCreatorIdAndTaskStatusCreated(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.status.name = 'INPROGRES')")
    List<Task> findAllByCreatorIdAndTaskStatusInProgres(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.status.name = 'REOPENED')")
    List<Task> findAllByCreatorIdAndTaskStatusReopened(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.status.name = 'END')")
    List<Task> findAllByCreatorIdAndTaskStatusEnd(@Param("id") Long id);


    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.priority.name = 'LOW')")
    List<Task> findAllByCreatorIdAndTaskPriorityLow(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.priority.name = 'NORMAL')")
    List<Task> findAllByCreatorIdAndTaskPriorityNormal(@Param("id") Long id);


    @Query("SELECT task FROM Task task WHERE task.creator.id = :id AND (task.priority.name = 'HIGH')")
    List<Task> findAllByCreatorIdAndTaskPriorityHigh(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.creator.id = :id")
    List<Task> findAllByCreatorId(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id")
    List<Task> findAllByExecutorId(@Param("id") Long id);

//    @Query("SELECT task FROM Task task " +
//            "where task.beginDate between ?1 and ?2 and b.endDate between ?1 and ?2")
//    List<Task> findTasksByDatesBetween(Date begin, Date end);

    //  queries for executor

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.status.name = 'OPENED')")
    List<Task> findAllByExecutorIdAndTaskStatusOpened(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.status.name = 'CREATED')")
    List<Task> findAllByExecutorIdAndTaskStatusCreated(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.status.name = 'INPROGRES')")
    List<Task> findAllByExecutorIdAndTaskStatusInProgres(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.status.name = 'REOPENED')")
    List<Task> findAllByExecutorIdAndTaskStatusReopened(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.status.name = 'END')")
    List<Task> findAllByExecutorIdAndTaskStatusEnd(@Param("id") Long id);


    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.priority.name = 'LOW')")
    List<Task> findAllByExecutorIdAndTaskPriorityLow(@Param("id") Long id);

    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.priority.name = 'NORMAL')")
    List<Task> findAllByExecutorIdAndTaskPriorityNormal(@Param("id") Long id);


    @Query("SELECT task FROM Task task WHERE task.executor.id = :id AND (task.priority.name = 'HIGH')")
    List<Task> findAllByExecutorIdAndTaskPriorityHigh(@Param("id") Long id);

}

