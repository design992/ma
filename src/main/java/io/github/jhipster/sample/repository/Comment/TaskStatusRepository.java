package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by PK on 2017-03-08.
 */
public interface TaskStatusRepository extends JpaRepository<TaskStatus, Long> {
    TaskStatus findByName(String name);
}

