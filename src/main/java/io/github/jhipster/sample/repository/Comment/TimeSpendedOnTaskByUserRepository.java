package io.github.jhipster.sample.repository.Comment;

import io.github.jhipster.sample.domain.Task;
import io.github.jhipster.sample.domain.TimeSpendedOnTaskByUser;
import io.github.jhipster.sample.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Created by PK on 2017-03-04.
 */
public interface TimeSpendedOnTaskByUserRepository extends JpaRepository<TimeSpendedOnTaskByUser, Long> {
    @Query("SELECT s FROM TimeSpendedOnTaskByUser s  JOIN FETCH s.user u  JOIN FETCH s.task t WHERE u = :user AND t = :task ")
    Optional<TimeSpendedOnTaskByUser> findAllByUserAndTask(@Param("user") User user, @Param("task") Task task);
}
