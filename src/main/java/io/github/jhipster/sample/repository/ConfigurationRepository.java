package io.github.jhipster.sample.repository;

import io.github.jhipster.sample.domain.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConfigurationRepository extends JpaRepository<Configuration, Long> {

    Optional<Configuration> findByCode(String code);

}
