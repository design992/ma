package io.github.jhipster.sample.repository;

import io.github.jhipster.sample.domain.BankAccount;
import io.github.jhipster.sample.domain.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback,Long> {

}
