package io.github.jhipster.sample.service;

import io.github.jhipster.sample.domain.Authority;
import io.github.jhipster.sample.repository.AuthorityRepository;
import io.github.jhipster.sample.security.AuthoritiesConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pkarwowski on 13.07.2017.
 */
@Service
@Transactional
public class AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;

    @Transactional(readOnly = true)
    public Set<Authority> getUserAuthority() {
        Set<Authority> authorities = new HashSet<>();
        authorities.add(authorityRepository.findOne(AuthoritiesConstants.USER));
        return authorities;
    }

    @Transactional(readOnly = true)
    public Set<Authority> getUserAndAdminAuthorities() {
        Set<Authority> authorities = new HashSet<>();
        authorities.add(authorityRepository.findOne(AuthoritiesConstants.USER));
        authorities.add(authorityRepository.findOne(AuthoritiesConstants.ADMIN));
        return authorities;
    }
}
