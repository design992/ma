package io.github.jhipster.sample.service;

import io.github.jhipster.sample.domain.Configuration;
import io.github.jhipster.sample.repository.ConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class ConfigurationService {

    public static final String CONF_CODE_MOTIVATORS_SIZE = "movingMotivatorsListSize";

    /** configuration repository. */
    @Autowired
    private ConfigurationRepository configurationRepository;

    /**
     * Zwraca wartosc pola feedback receiver email.
     *
     * @return wartosc pola feedback receiver email
     */
    @Transactional(readOnly = true)
    public String getFeedbackReceiverEmail() throws NoSuchElementException {
        Optional<Configuration> conf = configurationRepository.findByCode(Configuration.FEEDBACK_RECEIVER_EMAIL);
        if (!conf.isPresent()) {
            throw new NoSuchElementException("The requested resource doesn't exist in database.");
        }
        return conf.get().getFeatureValue();
    }

    /**
     * Zwraca wartosc ktorej odpowiada kod przekazany w parametrze
     *
     * @return wartosc pola
     */
    public String findByCode(String code) {
        Optional<Configuration> configuration = configurationRepository.findByCode(code);
        if (!configuration.isPresent()) {
            throw new NoSuchElementException("The requested resource doesn't exist in database.");
        }
        return configuration.get().getFeatureValue();
    }

    /**
     * Zwraca wartosc po sparsowaniu, ktorej odpowiada kod przekazany w parametrze
     *
     * @return wartosc pola sparsowana na typ Integer
     *
     * @throws NumberFormatException
     */
    public Integer findIntegerByCode(String code) {
        Integer parsedValue = null;
        final String value = findByCode(code);
        try {
            parsedValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Nieudana proba parsowania wartosci konfiguracji: " + value);
        }

        return parsedValue;
    }

}
