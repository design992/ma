package io.github.jhipster.sample.service;

import io.github.jhipster.sample.domain.Feedback;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.FeedbackRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class FeedbackService {

    private static final Logger log = LoggerFactory.getLogger(FeedbackService.class);
    private UserRepository userRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    private Feedback feedback = null;

    public Feedback addNewFeedback(String feedbackText) {
        feedback = new Feedback();
        feedback.setCreatedBy(SecurityUtils.getCurrentUserLogin());
        feedback.setMessage(feedbackText);
        return feedbackRepository.save(feedback);
    }
}
