package io.github.jhipster.sample.service;

import io.github.jhipster.sample.domain.Company;
import io.github.jhipster.sample.domain.User;

import io.github.jhipster.config.JHipsterProperties;

import io.github.jhipster.sample.repository.Comment.CompanyRepository;
import io.github.jhipster.sample.repository.UserRepository;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    @Autowired
    private UserService userService;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserRepository userRepository;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }
    @Async
    public void sendEmail(String[]  to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }
//    @Async
//    public void sendEmailWithCC(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
//        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
//            isMultipart, isHtml, to, subject, content);
//
//        // Prepare message using a Spring helper
//        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//        try {
//            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
//            message.setTo(to);
//            message.setFrom(jHipsterProperties.getMail().getFrom());
//            message.setSubject(subject);
//            message.setText(content, isHtml);
//            javaMailSender.send(mimeMessage);
//            log.debug("Sent email to User '{}'", to);
//        } catch (Exception e) {
//            if (log.isDebugEnabled()) {
//                log.warn("Email could not be sent to user '{}'", to, e);
//            } else {
//                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
//            }
//        }
//    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);

    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "passwordResetEmail", "email.reset.title");
    }

    @Async
    public void sendFeedbackEmail(User sender, String receiver, String baseUrl, String feedbackText)
        throws MessagingException {
        log.debug("Sending e-mail with feedback to '{}'", receiver);
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable("sender", sender);
        context.setVariable("receiver", receiver);
        context.setVariable("baseUrl", baseUrl);
        context.setVariable("feedbackText", feedbackText);
        String content = templateEngine.process("feedbackEmail", context);
//        String subject = messageSource.getMessage("email.feedback.title", null, locale);
        String subject = "Feedback";
        sendEmail(receiver, subject, content, false, true);
        log.debug("Sent e-mail with feedback to '{}'", receiver);
    }

    @Async
    public void sendInvitationEmail(User sender, User user, String baseUrl) throws MessagingException {
        log.debug("Sending invitation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable("sender", sender);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("invitationEmail", context);
        String subject = messageSource.getMessage("email.invitation.title", null, locale);

        sendEmail(user.getEmail(), subject, content, false, true);
        log.debug("Sent invitation e-mail to User '{}'", user.getEmail());
    }

    @Async
    public void sendKudoEmail(User sender, User user, String baseUrl) throws MessagingException {
        log.debug("Sending kudo e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable("sender", sender);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("kudoEmail", context);
        String subject = messageSource.getMessage("Kudo Card", null, locale);

        sendEmail(user.getEmail(), subject, content, false, true);
        log.debug("Sent invitation e-mail to User '{}'", user.getEmail());

    }

    @Async
    public void sendDailyEmail(String[] sender, String baseUrl) throws MessagingException {
//        log.debug("Sending invitation e-mail to '{}'", user.getEmail());
//        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Locale locale = Locale.forLanguageTag("EN");
        Context context = new Context(locale);
        context.setVariable("sender", sender);
//        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("dailyEmail", context);
        String subject = messageSource.getMessage("email.invitation.title", null, locale);

        sendEmail(sender, subject, content, false, true);
//        log.debug("Sent invitation e-mail to User '{}'", user.getEmail());
    }

//    @Scheduled(cron = "* 0 0 9 * * MON-FRI")
    @Scheduled(cron = "*/30 * * * * *")
    public void sendMail() throws MessagingException {
        List<Company> companies = companyRepository.findAll();
        for (int i = 0; i < companies.size(); i++) {
            List<User> activeUsersList = userRepository.findAllByCompanyId(companies.get(i).getId());
            List<String> emails = new ArrayList<>();
            for (User u : activeUsersList) {
                emails.add(u.getEmail());
            }
            String[] emailStr = emails.stream().toArray(String[]::new);
            sendDailyEmail(emailStr, "http://localhost:8080");
        }
    }
}
