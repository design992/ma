package io.github.jhipster.sample.service;

import io.github.jhipster.sample.config.Constants;
import io.github.jhipster.sample.domain.Authority;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.AuthorityRepository;
import io.github.jhipster.sample.repository.PersistentTokenRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.AuthoritiesConstants;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.dto.UserDTO;
import io.github.jhipster.sample.service.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final PersistentTokenRepository persistentTokenRepository;

    private final AuthorityRepository authorityRepository;

    @Autowired
    private AuthorityService authorityService;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, PersistentTokenRepository persistentTokenRepository, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.persistentTokenRepository = persistentTokenRepository;
        this.authorityRepository = authorityRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);

        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                return user;
            });
    }

    public User createUser(String login, String password, String firstName, String lastName, String email,
                           String imageUrl, String langKey) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setImageUrl(imageUrl);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setImageUrl(userDTO.getImageUrl());
        user.setCompany(userDTO.getCompany());
        if (userDTO.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = new HashSet<>();
            userDTO.getAuthorities().forEach(
                authority -> authorities.add(authorityRepository.findOne(authority))
            );
            user.setAuthorities(authorities);
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);

        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName  last name of user
     * @param email     email id of user
     * @param langKey   language key
     * @param imageUrl  image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String nextProject, String motivation,String certificates, String imageUrl) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setLangKey(langKey);
            user.setNextProject(nextProject);
            user.setMotivation(motivation);
            user.setCertificates(certificates);
            user.setImageUrl(imageUrl);
            log.debug("Changed Information for User: {}", user);
        });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                user.setLogin(userDTO.getLogin());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                user.setNextProject(userDTO.getNextProject());
                user.setMotivation(userDTO.getMotivation());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findOne)
                    .forEach(managedAuthorities::add);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            String encryptedPassword = passwordEncoder.encode(password);
            user.setPassword(encryptedPassword);
            log.debug("Changed password for User: {}", user);
        });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        return userRepository.findOneWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
        }
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    public User findOne(Long id) {
        return userRepository.findOne(id);
    }

    public User getLoggedUser() {
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        return loggedUser.get();
    }

    public Long getLoggedUserCompanyId() {
        return getLoggedUser().getCompany().getId();
    }
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }
    public boolean checkIfUniqueEmail(String email) {
        return userRepository.findOneByEmail(email).isPresent();
    }

    public boolean checkIfUniqueLogin(String login) {
        return userRepository.findOneByLogin(login).isPresent();
    }

//    public Optional<User> activateUserAndUpdateData(String login, String password, String key) throws Exception {
//        log.debug("Activating user for activation key {}", key);
//        Optional<User> optionalUser = activate(key);
//        if (optionalUser.isPresent()) {
//            User user = optionalUser.get();
//
//            if (user.getUserState().equals(UserState.INVITED)) {
//                if (user.getAuthorities().equals(authorityService.getUserAuthority())) {
//                    user.setLogin(login);
//                    user.setPassword(passwordEncoder.encode(password));
//                    user.setUserState(UserState.ACTIVE);
//                    userRepository.save(user);
//                    log.debug("Activated user and updated data: {}", user);
//                    return optionalUser;
//                } else {
//                    throw new AccessDeniedException("Access Denied.Activation key incorrect!");
//                }
//            } else {
//                throw new Exception("You can't activate deleted account!");
//            }
//        } else {
//            throw new Exception("You can't activate deleted account!");
//        }
//    }
//    public Optional<User> activate(String key) throws Exception {
//        log.debug("Active user with key:", key);
//        Optional<User> optionalUser = userRepository.findOneByActivationKey(key);
//        if (!optionalUser.isPresent()) {
//            throw new Exception("Key not found!");
//        } else if (optionalUser.get().getUserState().equals(UserState.INVITED)) {
//            // activate given user for the registration key.
//            User user = optionalUser.get();
//            user.setActivated(true);
//            user.setActivationKey(null);
////            user.setActivationDate(new DateTime());
//            userRepository.save(user);
//            log.debug("Activated user : {}", user);
//            return optionalUser;
//        } else {
//            throw new Exception("You can't activate deleted account!");
//        }
//    }
//    public User createUser(User newUser) throws Exception {
//        if (isThereOtherUserWithThisEmail(newUser)) {
//            throw new Exception("Is there other user with the same email address");
//        }
//        setUserReadyForActivation(newUser);
//        newUser.setAuthorities(authorityService.getUserAuthority());
//        return userRepository.save(newUser);
//    }
    public boolean isThereOtherUserWithThisEmail(User user) {
        Optional<User> user2 = userRepository.findOneByEmail(user.getEmail());
        return ((user2.isPresent()) && !(user2.get().getId().equals(user.getId())));
    }

    public List<User> getAllUsersFromLoggedUserCompany() {
        log.debug("Get all Users from Company  : {}");
        return userRepository.findAllByCompanyId(getLoggedUserCompanyId());
    }

    public List<User> getActiveUsersFromLoggedUserCompany() {
        return userRepository.findAllByCompanyIdAndStatus(getLoggedUserCompanyId());
    }
//    public User setUserReadyForActivation(User user) {
//        user.setActivated(true);
//        user.setActivationKey(RandomUtil.generateActivationKey());
////        user.setActivationDate(null);
////        user.setDeactivationDate(null);
//        user.setUserState(UserState.INVITED);
//        log.info("Updated Information for User: {}", user);
//        return user;
//    }
}
