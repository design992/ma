package io.github.jhipster.sample.service.comment;

import io.github.jhipster.sample.domain.Comment;
import io.github.jhipster.sample.domain.Company;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.CommentRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by PK on 2017-03-04.
 */
@Service
@Transactional
public class CommentService {

    private final static Logger log = LoggerFactory.getLogger(CommentService.class);

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserRepository userRepository;

    public Comment create(Comment comment) {
        log.debug("Creating comment {}", comment);
        User loggedUser = getLoggedUser();
//        comment.setPostDate(comment.getLastModifiedDate());
        comment.setUser(loggedUser);
//        comment.setPostDate(new DateTime());
//        if (comment.getId() != null) {

//            throw new IllegalArgumentException("Updating comment is not allowed");
//        }
        Comment c = commentRepository.save(comment);
        log.debug("Comment has been created {}", c);
        return c;
    }

    public void delete(Comment comment) {
        log.debug("Deleting Comment {}", comment);
        commentRepository.delete(comment);
        log.debug("Comment has been deleted {}", comment);
    }

    public Comment updateCommentData(Comment comment) {
        User loggedUser = getLoggedUser();
        Instant instant = Instant.now();
        comment.setUser(loggedUser);
//        comment.setPostDate(instant);

        return commentRepository.save(comment);
    }

    private User getLoggedUser() {
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        return loggedUser.get();
    }

    @Transactional(readOnly = true)
    public Comment findOne(Long id) {
        return commentRepository.findOne(id);
    }

    public Comment findAllCommentsByUserId(Long id) {
        Company company = userService.findOne(id).getCompany();
        return commentRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Comment> findAllByUserId(Long id) {
        return commentRepository.findAllByUserId(id);
    }

    public List<Comment> findAllCommentsFromLoggedUserCompany() {
        Long loggedUserCompanyId = userService.getLoggedUserCompanyId();
        List<Comment> allCommentsList = commentRepository.findAll();
        List<Comment> allCommentsFromCurrentCompany = new ArrayList<>();
        for (Comment c : commentRepository.findAll()) {
            if (loggedUserCompanyId.equals(c.getUser().getCompany().getId())) {
                allCommentsFromCurrentCompany.add(c);
            }
        }
        return allCommentsFromCurrentCompany;
    }

//    public Set<Comment> findAllCommentsFromLoggedUserTeams() {
//        List<Comment> allCommentsList = commentRepository.findAll();
//        Set<Comment> allCommentsFromCurrentUserTeams = new HashSet<>();
//        List<User> activeUsersFromLoggedUserCompany = userService.getActiveUsersFromLoggedUserCompany();
//        Set<Team> teamsOfCurrentUser = userService.getLoggedUser().getTeams();
//
//        for (Comment c : allCommentsList) {
//            if (CollectionUtils.containsAny(teamsOfCurrentUser, c.getUser().getTeams())) {
//                allCommentsFromCurrentUserTeams.add(c);
//            }
//        }
//
//        return allCommentsFromCurrentUserTeams;
//    }
    public List<Comment> findAllByUserLogin(String login){
        List<Comment> allByUserLogin = commentRepository.findAllByUserLogin(login);

        return allByUserLogin;
    }
    public List<Comment> findAllCommentsByUser(User user){
        List<Comment> allCommentsByUser = commentRepository.findAllByUser(user);
        return allCommentsByUser;
    }
}
