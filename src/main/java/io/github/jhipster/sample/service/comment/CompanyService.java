/*
 *
 */
package io.github.jhipster.sample.service.comment;

import io.github.jhipster.sample.domain.Company;
import io.github.jhipster.sample.repository.Comment.CompanyRepository;
import io.github.jhipster.sample.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class CompanyService {
    private static final Logger log = LoggerFactory.getLogger(CompanyService.class);
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserRepository userRepository;

    public Company findOne(Long id) {
        return companyRepository.getOne(id);
    }


//    @Transactional(readOnly = true)
//    public List<Company> getAllWithDependencies() {
//        return companyRepository.findAllCompanies();
//    }

    public Company findOneByUserId(Long id) {
        return companyRepository.findOneByUserId(id).get();
    }

    public Company saveCompanyInfo(Company newCompanyInfo, Long userLoggedId) {

        Company companyInfo = findOneByUserId(userLoggedId);

        companyInfo.setContactPerson(newCompanyInfo.getContactPerson());
        companyInfo.setName(newCompanyInfo.getName());
        companyInfo.setWebsite(newCompanyInfo.getWebsite());
        companyInfo.setPhone(newCompanyInfo.getPhone());
        companyInfo.setVersion(newCompanyInfo.getVersion());

        return companyRepository.save(companyInfo);
    }

    public Company createNewCompanyWithDefaultPlan() {
        log.debug("on enter createNewCompanyWithDefaultPlan");

        Company company = new Company();
        company = companyRepository.save(company);

        log.info("Company created: ", company);
        log.debug("on exit createNewCompanyWithDefaultPlan");
        return company;
    }

}
