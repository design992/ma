package io.github.jhipster.sample.service.comment;

import io.github.jhipster.sample.domain.KudoCard;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.KudoCardRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by PK on 2017-03-08.
 */
@Service
@Transactional
public class KudoCardService {
    private final static Logger log = LoggerFactory.getLogger(KudoCardService.class);

    @Autowired
    private KudoCardRepository kudoCardRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private KudoCardService kudoCardService;
    @Autowired
    private UserRepository userRepository;

    public KudoCard create(KudoCard kudoCard) {
        log.debug("Creating kudoCard {}", kudoCard);

        KudoCard c = kudoCardRepository.save(kudoCard);
        log.debug("kudoCard has been created {}", c);
        return c;
    }

    public void delete(KudoCard kudoCard) {
        log.debug("Deleting kudoCard {}", kudoCard);
        kudoCardRepository.delete(kudoCard);
        log.debug("kudoCard has been deleted {}", kudoCard);
    }
    public void delete(Long id) {
        KudoCard kudoCard = kudoCardRepository.findOne(id);
        log.debug("Deleting kudoCard {}", kudoCard);
        kudoCardRepository.delete(kudoCard);
        log.debug("kudoCard has been deleted {}", kudoCard);
    }

    public void save(KudoCard kudoCard) {
        log.debug("Save kudoCard {}", kudoCard);
        kudoCardRepository.save(kudoCard);
        log.debug("kudoCard has been saved {}", kudoCard);
    }

    @Transactional(readOnly = true)
    public KudoCard findOne(Long id) {
        return kudoCardRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<KudoCard> findAll() {
        return kudoCardRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<KudoCard> findAllKudoCardSentByCurrentLoggedUser() {
        User loggedUser = getLoggedUser();
        return kudoCardRepository.findAllByFromUserId(loggedUser.getId());
    }

    @Transactional(readOnly = true)
    public List<KudoCard> findAllKudoCardReceivedByCurrentLoggedUser() {
        User user = getLoggedUser();
        return kudoCardRepository.findAllByToUserId(user.getId());
    }

    private User getLoggedUser() {
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        return loggedUser.get();
    }
}
