package io.github.jhipster.sample.service.comment;

import io.github.jhipster.sample.domain.Skill;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.SkillRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by PK on 2017-03-08.
 */
@Service
@Transactional
public class SkillService {
    private final static Logger log = LoggerFactory.getLogger(SkillService.class);

    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SkillService skillService;

    public Skill create(Skill skill) {
        log.debug("Creating skill {}", skill);

        Skill c = skillRepository.save(skill);
        log.debug("skill has been created {}", c);
        return c;
    }

    public void delete(Skill skill) {
        log.debug("Deleting skill {}", skill);
        skillRepository.delete(skill);
        log.debug("skill has been deleted {}", skill);
    }
    public void save(Skill skill) {
        log.debug("Save skill {}", skill);
        skillRepository.save(skill);
        log.debug("skill has been saved {}", skill);
    }

    @Transactional(readOnly = true)
    public Skill findOne(Long id) {
        return skillRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public Skill findByName(String name) {
        return skillRepository.findByName(name);
    }
    @Transactional(readOnly = true)
    public List<Skill> findAll() {
        return skillRepository.findAll();
    }
    @Transactional(readOnly = true)
    public List<Skill> findAllSkillsOfCurrentLoggedUser() {
        User loggedUser = getLoggedUser();
        return skillRepository.findByUser(loggedUser);
    }
    @Transactional(readOnly = true)
    public List<Skill> findAllSkillsOfUSerById(Long id) {
        User user = userService.findOne(id);
        return skillRepository.findByUser(user);
    }
//    @Transactional(readOnly = true)
//    public Skill findSkillById(Long id) {
//        Skill skill = skillRepository.findOne(id);
//        return skill;
//    }
    private User getLoggedUser() {
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        return loggedUser.get();
    }
}
