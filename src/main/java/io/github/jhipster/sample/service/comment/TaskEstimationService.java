package io.github.jhipster.sample.service.comment;

import io.github.jhipster.sample.domain.TaskEstimation;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.TaskEstimationRepository;
import io.github.jhipster.sample.repository.Comment.TaskRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by PK on 2017-03-08.
 */
@Service
@Transactional
public class TaskEstimationService {
    private final static Logger log = LoggerFactory.getLogger(TaskEstimationService.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private TaskEstimationRepository taskEstimationRepository;
    @Autowired
    private UserRepository userRepository;

    public TaskEstimation add(TaskEstimation taskEstimation) {
        log.debug("Creating estimation for task {}", taskEstimation);

        taskEstimation.setUser(getLoggedUser());

        TaskEstimation taskEstimation1 = taskEstimationRepository.save(taskEstimation);
        log.debug("Task has been created {}", taskEstimation1);
        return taskEstimation;
    }

    private User getLoggedUser() {
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        return loggedUser.get();
    }

    public void delete(TaskEstimation taskEstimation) {
        log.debug("Deleting task {}", taskEstimation);
        taskEstimationRepository.delete(taskEstimation);
        log.debug("task estimation  has been deleted {}", taskEstimation);
    }


    @Transactional(readOnly = true)
    public TaskEstimation findOne(Long id) {
        return taskEstimationRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<TaskEstimation> findAll() {
        return taskEstimationRepository.findAll();
    }

}
