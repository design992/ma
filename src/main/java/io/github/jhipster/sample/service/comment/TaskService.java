package io.github.jhipster.sample.service.comment;

import io.github.jhipster.sample.domain.Task;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.TaskRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by PK on 2017-03-08.
 */
@Service
@Transactional
public class TaskService {
    private final static Logger log = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    public Task create(Task task) {
        log.debug("Creating task {}", task);

        task.setCreator(getLoggedUser());
//        task.setCreatedDate(new DateTime());
//        task.setTaskStatus(TaskStatus.CREATED);
        Task c = taskRepository.save(task);
        log.debug("Task has been created {}", c);
        return c;
    }

    public void delete(Task task) {
        log.debug("Deleting task {}", task);
        taskRepository.delete(task);
        log.debug("task has been deleted {}", task);
    }
    public void save(Task task) {
        log.debug("Save task {}", task);
        taskRepository.save(task);
        log.debug("task has been saved {}", task);
    }

    @Transactional(readOnly = true)
    public Task findOne(Long id) {
        return taskRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public List<Task> findAll() {
        return taskRepository.findAll();
    }
    @Transactional(readOnly = true)
    public List<Task> findAllTasksOfCurrentLoggedUserCompany() {
        User loggedUser = getLoggedUser();
        List<Task> tasks = taskRepository.findAll();
        List<Task> tasksFromCompany= new ArrayList<>();
        for(int i =0; i<tasks.size(); i++){
            if(loggedUser.getCompany().getId().equals(loggedUser.getCompany().getId())){
                tasksFromCompany.add(tasks.get(i));
            }
        }
        return tasksFromCompany;
    }

    public List<Task> findAllTasksOfCurrentLoggedUser(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByUserId(loggedUser.getId());
        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskPriority().getName().equals("HIGH")).collect(Collectors.toList());
        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskPriority().getName().equals("LOW")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskPriority().getName().equals("NORMAL")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;
    }

    public List<Task> findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByExecutorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskPriority().getName().equals("HIGH")).collect(Collectors.toList());
        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByExecutorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskPriority().getName().equals("LOW")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByExecutorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskPriority().getName().equals("NORMAL")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;
    }

    public List<Task> findAllByCreatorIdAndTaskStatusCreated(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("CREATED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllByCreatorIdAndTaskStatusOpened(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("OPENED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
    public List<Task> findAllByCreatorIdAndTaskStatusInprogress(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("INPROGRESS")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;
    }
    public List<Task> findAllByCreatorIdAndTaskStatusReopened(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("REOPENED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
    public List<Task> findAllByCreatorIdAndTaskStatusEnd(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("CLOSED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }

    //
    public List<Task> findAllByCurrentUserExecutorIdAndTaskStatusCreated(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("CREATED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
    public List<Task> findAllByCurrentUserExecutorIdAndTaskStatusOpened(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("OPENED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
    public List<Task> findAllByCurrentUserExecutorIdAndTaskStatusInprogress(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("INPROGRESS")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
    public List<Task> findAllByCurrentUserExecutorIdAndTaskStatusReopened(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("REOPENED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
    public List<Task> findAllByCurrentUserExecutorIdAndTaskStatusEnd(){
        User loggedUser = getLoggedUser();
        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
        allTaksCreatedByCurrentUser = allTaksCreatedByCurrentUser.stream().filter(t-> t.getTaskStatus().getName().equals("CLOSED")).collect(Collectors.toList());

        return allTaksCreatedByCurrentUser;

    }
//    public List<Task> findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh(){
//        User loggedUser = getLoggedUser();
//        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByCreatorId(loggedUser.getId());
//        return allTaksCreatedByCurrentUser;
//    }
//    public List<Task> findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow(){
//        User loggedUser = getLoggedUser();
//        List<Task> allTaksCreatedByCurrentUser = taskRepository.findAllByExecutorIdAndTaskPriorityLow(loggedUser.getId());
//        return allTaksCreatedByCurrentUser;
//    }

    private User getLoggedUser() {
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        return loggedUser.get();
    }

    public void writeXmlFile() {

        try {
            List<Task> all = findAllTasksOfCurrentLoggedUserCompany();
            DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
            DocumentBuilder build = dFact.newDocumentBuilder();
            Document doc = build.newDocument();

            Element root = doc.createElement("TaskInfo");
            doc.appendChild(root);

            Element nameLabel = doc.createElement("Tasks");
            root.appendChild(nameLabel);


            for (Task dtl : all) {
                Element task = doc.createElement("Task");


                Element name = doc.createElement("Name");
                name.appendChild(doc.createTextNode(String.valueOf(dtl
                        .getName())));
                task.appendChild(name);

                Element id = doc.createElement("ID");
                id.appendChild(doc.createTextNode(String.valueOf(dtl.getId())));
                task.appendChild(id);

                Element desc = doc.createElement("Description");
                desc.appendChild(doc.createTextNode(String.valueOf(dtl
                        .getDescription())));
                task.appendChild(desc);


                Element creator = doc.createElement("Creator");
                creator.appendChild(doc.createTextNode(String.valueOf(dtl.getCreator().toString())));
                task.appendChild(creator);

//                Element beginDate = doc.createElement("BeginDate");
//                beginDate.appendChild(doc.createTextNode(String.valueOf(dtl.getBeginDate().toString())));
//                task.appendChild(beginDate);

//                Element endDate = doc.createElement("EndDate");
//                endDate.appendChild(doc.createTextNode(String.valueOf(dtl.getEndDate().toString())));
//                task.appendChild(endDate);

                nameLabel.appendChild(task);
            }

            // Save the document to the disk file
            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            // format the XML nicely
            aTransformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");

            aTransformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "4");
            aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(doc);
            try {
                // location and name of XML file you can change as per need
                FileWriter fos = new FileWriter("./tasks.xml");
                StreamResult result = new StreamResult(fos);
                aTransformer.transform(source, result);

            } catch (IOException e) {

                e.printStackTrace();
            }

        } catch (TransformerException ex) {
            System.out.println("Error outputting document");

        } catch (ParserConfigurationException ex) {
            System.out.println("Error building document");
        }
    }
}
