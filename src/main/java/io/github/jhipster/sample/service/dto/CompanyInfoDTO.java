package io.github.jhipster.sample.service.dto;

import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by pkarwowski on 14.07.2017.
 */
public class CompanyInfoDTO {

    @NotNull
    private Long companyContactPerson;

    @NotNull
    @Size(min = 1, max = 50)
    private String companyName;
    @Size(min = 0, max = 50)
    private String companyWebsite;
    @Size(min = 0, max = 20)
    private String companyPhone;
    @Version
    private long companyVersion;

    public CompanyInfoDTO() {
    }

    public CompanyInfoDTO(Long companyContactPerson, String companyName, String companyWebsite, String companyPhone,
                          Long companyVersion) {
        this.companyContactPerson = companyContactPerson;
        this.companyName = companyName;
        this.companyWebsite = companyWebsite;
        this.companyPhone = companyPhone;
        this.companyVersion = companyVersion;
    }

    public Long getCompanyContactPerson() {
        return companyContactPerson;
    }

    public void setCompanyContactPerson(Long companyContactPerson) {
        this.companyContactPerson = companyContactPerson;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public long getCompanyVersion() {
        return companyVersion;
    }

    public void setCompanyVersion(long companyVersion) {
        this.companyVersion = companyVersion;
    }

    @Override
    public String toString() {
        return "CompanyInfoDTO [companyContactPerson=" + companyContactPerson + ", companyName=" + companyName
            + ", companyWebsite=" + companyWebsite + ", companyPhone=" + companyPhone + ", companyCountry=" + ", companyVersion=" + companyVersion + "]";
    }

}
