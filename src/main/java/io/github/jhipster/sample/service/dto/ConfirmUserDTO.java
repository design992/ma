package io.github.jhipster.sample.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by pkarwowski on 13.07.2017.
 */
public class ConfirmUserDTO {

    @Pattern(regexp = "^[A-Za-z0-9._+@-]*$")
    @NotNull
    @Size(min = 1, max = 50)
    private String login;

    @NotNull
    @Size(min = 5, max = 100)
    private String password;

    @Size(max = 20)
    private String key;

    public ConfirmUserDTO() {
        super();
    }

    public ConfirmUserDTO(String login, String password, String key) {
        super();
        this.login = login;
        this.password = password;
        this.key = key;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "ConfirmUserDTO [login=" + login + ", password=" + password + ", key=" + key + "]";
    }

}

