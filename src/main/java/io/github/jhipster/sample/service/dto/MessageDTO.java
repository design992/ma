package io.github.jhipster.sample.service.dto;

public class MessageDTO {
    public enum Message {
        LOGIN_MESSAGE_ERROR("login.messages.error.authentication"),
        BILLINGINFO_MESSAGES_SUCCESS("billingInfo.messages.success"),
        GLOBAL_NOTIFY_ACCDELETED("global.notify.accdeleted"),
        GLOBAL_NOTIFY_ACCOUNTCREATED("global.notify.accountCreated"),
        GLOBAL_NOTIFY_ALREADYINTEAM("global.notify.alreadyInTeam"),
        GLOBAL_NOTIFY_DEACTIVATED("global.notify.deactivated"),
        GLOBAL_NOTIFY_LOCK("global.notify.lock"),
        GLOBAL_NOTIFY_LOGININUSE("global.notify.loginInUse"),
        GLOBAL_NOTIFY_MAIL_IN_USE("global.notify.mailInUse"),
        GLOBAL_NOTIFY_NOUSER("global.notify.noUser"),
        GLOBAL_NOTIFY_NOAUTH("global.notify.noauth"),
        GLOBAL_NOTIFY_NOTINTEAM("global.notify.notInTeam"),
        GLOBAL_NOTIFY_NOTEAM("global.notify.noTeam"),
        GLOBAL_NOTIFY_PERSONINVITED("global.notify.personInvited"),
        GLOBAL_NOTIFY_SAVED("global.notify.saved"),
        GLOBAL_NOTIFY_TEAMEXISTS("global.notify.teamExists"),
        GLOBAL_NOTIFY_UNDEFINEDERROR("global.notify.undefinedError"),
        GLOBAL_NOTIFY_WELCOMEINAPP("global.notify.welcomeInApp"),
        GLOBAL_NOTIFY_WRONGACTIVATIONLINK("global.notify.wrongActivationLink"),
        GLOBAL_NOTIFY_BILLINGSAVED("global.notify.billingsaved"),
        GLOBAL_NOTIFY_TEAMDELETED("global.notify.teamDeleted"),
        GLOBAL_NOTIFY_NOTFOUND("global.notify.notFound"),
        GLOBAL_NOTIFY_ERRORSAVINGDATA("global.notify.errorSavingData"),
        GLOBAL_NOTIFY_BADREQUEST("global.notify.badRequest"),
        GLOBAL_NOTIFY_SAVEDPHOTO("global.notify.savedphoto"),
        GLOBAL_NOTIFY_USER_NOT_ACTIVATED("global.notify.userNotActivated"),
        GLOBAL_NOTIFY_ACTIVATION_KEY_INCORRECT("global.notify.activationKeyIncorrect"),
        GLOBAL_NOTIFY_LASTADMIN("global.notify.lastAdmin"),
        GLOBAL_KUDO_ASSIGN("global.kudo.assign"),
        GLOBAL_NOTIFY_ACCESSDENIED("global.notify.accessDenied"),
        GLOBAL_NOTIFY_SKILLALREADYKNOWN("global.notify.skillAlreadyKnown"),
        GLOBAL_NOTIFY_SKILLDELETED("global.notify.skillDeleted"),
        GLOBAL_NOTIFY_MOVINGMOTIVATORS_INCORRECT("global.notify.motivatorsIncorrect");


        private String notificationId;

        Message(String notificationId){
            this.notificationId = notificationId;
        }

        public String getNotificationId() {
            return notificationId;
        }

        @Override
        public String toString() {
            return notificationId;
        }
    }

    private String code;

    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public MessageDTO(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public MessageDTO() {
        super();
    }

    @Override
    public String toString() {
        return "MessageDTO [code=" + code + ", Message=" + message + "]";
    }

}

