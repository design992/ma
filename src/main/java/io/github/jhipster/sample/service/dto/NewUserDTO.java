package io.github.jhipster.sample.service.dto;

import io.github.jhipster.sample.config.Constants;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by pkarwowski on 13.07.2017.
 */
public class NewUserDTO {

    @NotNull
    @Size(min = 1, max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

    @Email
    @Size(min = 5, max = 100)
    private String email;

    @Size(min = 2, max = 5)
    private String langKey;

    @Size(max = 50)
    private String jobTitle;
    private String certificates;
    public NewUserDTO() {
        super();
    }

    public NewUserDTO(String firstName, String lastName, String email, String langKey, String jobTitle,String login,String certificates) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.langKey = langKey;
        this.jobTitle = jobTitle;
        this.login=login;
        this.certificates=certificates;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    @Override
    public String toString() {
        return "NewUserDTO [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", langKey="
            + langKey + ", jobTitle=" + jobTitle + "]";
    }

}
