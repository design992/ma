package io.github.jhipster.sample.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by pkarwowski on 12.07.2017.
 */
public class SimpleUserDTO {

    /** id. */
    private Long id;

    /** login. */
    @Pattern(regexp = "^[A-Za-z0-9._+@-]*$")
    @NotNull
    @Size(min = 1, max = 50)
    private String login;

    /** first name. */
    @Size(max = 50)
    private String firstName;

    private boolean hasPhoto;
    private String certificates;

    public SimpleUserDTO() {
        super();
    }

    public SimpleUserDTO(Long id, String login, String firstName, boolean hasPhoto,String certificates) {
        super();
        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.hasPhoto = hasPhoto;
        this.certificates=certificates;
    }

    public boolean isHasPhoto() {
        return hasPhoto;
    }

    public void setHasPhoto(boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    @Override
    public String toString() {
        return "SimpleUserDTO [id=" + id + ", login=" + login + ", firstName=" + firstName + ", hasPhoto=" + hasPhoto
            + "]";
    }

}
