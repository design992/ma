package io.github.jhipster.sample.service.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by pkarwowski on 13.07.2017.
 */
public class SkillDTO {

    private Long id;

    @NotNull
    private SimpleUserDTO user;

    @NotNull
    private String name;

    public SkillDTO() {

    }

    public SkillDTO(Long id, SimpleUserDTO user, String name) {
        this.id = id;
        this.user = user;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SimpleUserDTO getUser() {
        return user;
    }

    public void setUser(SimpleUserDTO user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SkillDTO{" +
            "id=" + id +
            ", user=" + user +
            ", name='" + name + '\'' +
            '}';
    }
}
