package io.github.jhipster.sample.service.dto;

import io.github.jhipster.sample.domain.*;

import java.time.Instant;
import java.util.List;

/**
 * Created by pkarwowski on 12.07.2017.
 */


public class TaskDTO {

    private Long id;
    private String name;
    private String description;
    private TaskStatus taskStatus;
    private TaskPriority taskPriority;
    private SimpleUserDTO creator;
    private Long creatorId;
    private SimpleUserDTO executor;
    private Instant beginDate;
    private Instant endDate;
    private Double spendedTime;
    private String status;
    private String priority;
    private List<Comment> posts;
    private List<TaskEstimation> taskEstimations;
    private List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser;
    private String certificates;
    private String comment;
    private Double estimation;
    private Double averageEstimation;
    private Double totalSpendedTime;
    private Boolean overtimed;

    public Double getAverageEstimation() {
        return averageEstimation;
    }

    public void setAverageEstimation(Double averageEstimation) {
        this.averageEstimation = averageEstimation;
    }

    public List<TimeSpendedOnTaskByUser> getTimeSpendedOnTaskByUser() {
        return timeSpendedOnTaskByUser;
    }

    public Double getEstimation() {
        return estimation;
    }

    public void setEstimation(Double estimation) {
        this.estimation = estimation;
    }

    public void setTimeSpendedOnTaskByUser(List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser) {
        this.timeSpendedOnTaskByUser = timeSpendedOnTaskByUser;
    }

    public Double getTotalSpendedTime() {
        return totalSpendedTime;
    }

    public void setTotalSpendedTime(Double totalSpendedTime) {
        this.totalSpendedTime = totalSpendedTime;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<TaskEstimation> getTaskEstimations() {
        return taskEstimations;
    }

    public void setTaskEstimations(List<TaskEstimation> taskEstimations) {
        this.taskEstimations = taskEstimations;
    }

    public List<Comment> getPosts() {
        return posts;
    }

    public void setPosts(List<Comment> posts) {
        this.posts = posts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TaskPriority getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(TaskPriority taskPriority) {
        this.taskPriority = taskPriority;
    }

    public SimpleUserDTO getCreator() {
        return creator;
    }

    public void setCreator(SimpleUserDTO creator) {
        this.creator = creator;
    }

    public SimpleUserDTO getExecutor() {
        return executor;
    }

    public void setExecutor(SimpleUserDTO executor) {
        this.executor = executor;
    }

    public Instant getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Instant beginDate) {
        this.beginDate = beginDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Double getSpendedTime() {
        return spendedTime;
    }

    public void setSpendedTime(Double spendedTime) {
        this.spendedTime = spendedTime;
    }

    public Boolean getOvertimed() {
        return overtimed;
    }

    public void setOvertimed(Boolean overtimed) {
        this.overtimed = overtimed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskDTO taskDTO = (TaskDTO) o;

        if (id != null ? !id.equals(taskDTO.id) : taskDTO.id != null) return false;
        if (name != null ? !name.equals(taskDTO.name) : taskDTO.name != null) return false;
        if (description != null ? !description.equals(taskDTO.description) : taskDTO.description != null) return false;
        if (taskStatus != null ? !taskStatus.equals(taskDTO.taskStatus) : taskDTO.taskStatus != null) return false;
        if (taskPriority != null ? !taskPriority.equals(taskDTO.taskPriority) : taskDTO.taskPriority != null)
            return false;
        if (creator != null ? !creator.equals(taskDTO.creator) : taskDTO.creator != null) return false;
        if (creatorId != null ? !creatorId.equals(taskDTO.creatorId) : taskDTO.creatorId != null) return false;
        if (executor != null ? !executor.equals(taskDTO.executor) : taskDTO.executor != null) return false;
        if (beginDate != null ? !beginDate.equals(taskDTO.beginDate) : taskDTO.beginDate != null) return false;
        if (endDate != null ? !endDate.equals(taskDTO.endDate) : taskDTO.endDate != null) return false;
        if (spendedTime != null ? !spendedTime.equals(taskDTO.spendedTime) : taskDTO.spendedTime != null) return false;
        if (status != null ? !status.equals(taskDTO.status) : taskDTO.status != null) return false;
        return priority != null ? priority.equals(taskDTO.priority) : taskDTO.priority == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (taskStatus != null ? taskStatus.hashCode() : 0);
        result = 31 * result + (taskPriority != null ? taskPriority.hashCode() : 0);
        result = 31 * result + (creator != null ? creator.hashCode() : 0);
        result = 31 * result + (creatorId != null ? creatorId.hashCode() : 0);
        result = 31 * result + (executor != null ? executor.hashCode() : 0);
        result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (spendedTime != null ? spendedTime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", taskStatus=" + taskStatus +
            ", taskPriority=" + taskPriority +
            ", creator=" + creator +
            ", executor=" + executor +
            ", beginDate=" + beginDate +
            ", endDate=" + endDate +
            ", status='" + status + '\'' +
            ", priority='" + priority + '\'' +
            '}';
    }
}


