package io.github.jhipster.sample.service.mapper;

import io.github.jhipster.sample.domain.Company;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.dto.CompanyInfoDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by pkarwowski on 14.07.2017.
 */
@Component
public class CompanyMapper {

    private ModelMapper mapper;
    @Autowired
    private UserService userService;

    public CompanyMapper() {
    }

    @PostConstruct
    protected final void init() {
        mapper = new ModelMapper();
        mapper.getConfiguration().setImplicitMappingEnabled(false);
        mapper.addMappings(new PropertyMap<Company, CompanyInfoDTO>() {

            @Override
            protected void configure() {
                map().setCompanyName(source.getName());
                map().setCompanyWebsite(source.getWebsite());
                map().setCompanyPhone(source.getPhone());
                map().setCompanyContactPerson(source.getContactPerson().getId());
                map().setCompanyVersion(source.getVersion());
            }
        });
        mapper.addMappings(new PropertyMap<CompanyInfoDTO, Company>() {

            @Override
            protected void configure() {
                map().setName(source.getCompanyName());
                map().setWebsite(source.getCompanyWebsite());
                map().setPhone(source.getCompanyPhone());
                map().setVersion(source.getCompanyVersion());
            }
        });

    }

    public CompanyInfoDTO convertToDto(Company source) {
        return mapper.map(source, CompanyInfoDTO.class);
    }

    public void updateEntityWithDTOData(CompanyInfoDTO source, Company destination) {
        mapper.map(source, destination);
        destination.setContactPerson(userService.findOne(source.getCompanyContactPerson()));
//        destination.setCountry(countryService.findOne(source.getCompanyCountry()));
    }

}
