package io.github.jhipster.sample.service.mapper;

import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.service.dto.SimpleUserDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by pkarwowski on 12.07.2017.
 */
@Component
public class SimpleUserMapper {
    private ModelMapper mapper;

    public SimpleUserMapper() {
    }
    @PostConstruct
    protected void init() {
        mapper = new ModelMapper();
        mapper.getConfiguration().setImplicitMappingEnabled(false);
        mapper.addMappings(new PropertyMap<User, SimpleUserDTO>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setLogin(source.getLogin());
                map().setFirstName(source.getFirstName());
                map().setCertificates(source.getCertificates());
            }
        });
        mapper.addMappings(new PropertyMap<SimpleUserDTO, User>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setLogin(source.getLogin());
                map().setFirstName(source.getFirstName());
                map().setCertificates(source.getCertificates());
            }
        });

    }

    public SimpleUserDTO convertToDto(User source) {
        SimpleUserDTO dto = mapper.map(source, SimpleUserDTO.class);
//        dto.setHasPhoto(source.getImage() != null);
        return dto;
    }
    public User convertFromDto(SimpleUserDTO source) {
        User dto = mapper.map(source, User.class);
//        dto.setHasPhoto(source.getImage() != null);
        return dto;
    }
    public void updateEntityWithDTOData(SimpleUserDTO source, User destination) {
        mapper.map(source, destination);
    }

}
