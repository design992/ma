package io.github.jhipster.sample.service.mapper;

import io.github.jhipster.sample.domain.Skill;
import io.github.jhipster.sample.service.dto.SkillDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pkarwowski on 13.07.2017.
 */

@Component
public class SkillMapper {

    private ModelMapper mapper;

//    @Autowired
//    private String n;

    @Autowired
    private SimpleUserMapper simpleUserMapper;

    public SkillMapper() {
    }

    @PostConstruct
    protected void init() {
        mapper = new ModelMapper();
        mapper.getConfiguration().setImplicitMappingEnabled(false);
        mapper.addMappings(new PropertyMap<Skill, SkillDTO>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setName(source.getName());
            }
        });
        mapper.addMappings(new PropertyMap<SkillDTO, Skill>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setName(source.getName());
            }
        });
    }

    public SkillDTO convertToDto(Skill source) {
        SkillDTO dto = mapper.map(source, SkillDTO.class);

//        dto.setSkillDictionary(skillDictionaryMapper.convertToDto(source.getSkillDictionary()));
//        dto.getName(source.getName());
        dto.setName(source.getName());
//        dto.setUser(simpleUserMapper.convertToDto(source.getUser()));

        return dto;
    }
    public Skill convertFromDto(SkillDTO source) {
        Skill dto = mapper.map(source, Skill.class);

//        dto.setSkillDictionary(skillDictionaryMapper.convertToDto(source.getSkillDictionary()));
//        dto.getName(source.getName());
        dto.setId(source.getId());
        dto.setName(source.getName());
//        dto.setUser(simpleUserMapper.convertFromDto(source.getUser()));

        return dto;
    }

    public List<SkillDTO> convertToDto(List<Skill> source) {
        List<SkillDTO> dtoList = new ArrayList<>();

        for (Skill s : source) {
            dtoList.add(convertToDto(s));
        }

        return dtoList;
    }

}
