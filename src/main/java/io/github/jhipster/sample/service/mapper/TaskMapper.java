//package io.github.jhipster.sample.service.mapper;
//
//import io.github.jhipster.sample.domain.Comment;
//import io.github.jhipster.sample.domain.Task;
//import io.github.jhipster.sample.domain.User;
//import io.github.jhipster.sample.repository.Comment.TaskEstimationRepository;
//import io.github.jhipster.sample.service.UserService;
//import io.github.jhipster.sample.service.comment.TaskService;
//import io.github.jhipster.sample.service.dto.SimpleUserDTO;
//import io.github.jhipster.sample.service.dto.TaskDTO;
//import org.modelmapper.ModelMapper;
//import org.modelmapper.PropertyMap;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
////import org.modelmapper.ModelMapper;
////import org.modelmapper.PropertyMap;
///**
// * Created by pkarwowski on 12.07.2017.
// */
//
//
//@Component
//public class TaskMapper {
////    @Autowired
//    private org.modelmapper.ModelMapper mapper;
//    @Autowired
//    private SimpleUserMapper simpleUserMapper;
//    @Autowired
//    private UserService userService;
//    @Autowired
//    private TaskService taskService;
//    @Autowired
//    private TaskEstimationRepository taskEstimationRepository;
//
//    public TaskMapper() {
//    }
//
//    @PostConstruct
//    protected final void init() {
//        mapper = new ModelMapper();
//        mapper.getConfiguration().setImplicitMappingEnabled(false);
//        mapper.addMappings(new PropertyMap<Task, TaskDTO>() {
//
//            @Override
//            protected void configure() {
//                map().setId(source.getId());
//                map().setName(source.getName());
//                map().setDescription(source.getDescription());
////                map().setBeginDate(source.getBeginDate());
////                map().setEndDate(source.getEndDate());
//                map().setTaskStatus(source.getTaskStatus());
//                map().setTaskPriority(source.getTaskPriority());
//                map().setStatus(source.getStatus());
//                map().setPriority(source.getPriority());
//                map().setPosts(source.getPosts());
//                map().setTaskEstimations(source.getTaskEstimations());
//                map().setTimeSpendedOnTaskByUser(source.getTimeSpendedOnTaskByUser());
//            }
//        });
//        mapper.addMappings(new PropertyMap<TaskDTO, Task>() {
//
//            @Override
//            protected void configure() {
//                map().setId(source.getId());
////                map().setBeginDate(source.getBeginDate());
////                map().setEndDate(source.getEndDate());
//                map().setDescription(source.getDescription());
//                map().setName(source.getName());
//                map().setTaskPriority(source.getTaskPriority());
//                map().setTaskStatus(source.getTaskStatus());
//                map().setPriority(source.getPriority());
//                map().setStatus(source.getStatus());
//                map().setPosts(source.getPosts());
//                map().setTaskEstimations(source.getTaskEstimations());
//                map().setTimeSpendedOnTaskByUser(source.getTimeSpendedOnTaskByUser());
//            }
//        });
//
//    }
//
//    public TaskDTO convertToDto(Task source) {
//        TaskDTO taskDTO = mapper.map(source, TaskDTO.class);
//        taskDTO.setId(source.getId());
//        List<Comment> posts = source.getPosts();
////        taskDTO.setPosts(posts);
//        taskDTO.setDescription(source.getDescription());
//        taskDTO.setName(source.getName());
////        taskDTO.setBeginDate(source.getBeginDate());
////        taskDTO.setEndDate(source.getEndDate());
//        taskDTO.setPriority(source.getPriority());
//        taskDTO.setStatus(source.getStatus());
//        taskDTO.setCreator(simpleUserMapper.convertToDto(source.getCreator()));
//
////        taskDTO.setTaskEstimations(source.getTaskEstimations());
////        taskDTO.setTimeSpendedOnTaskByUser(source.getTimeSpendedOnTaskByUser());
//        if (source.getExecutor() != null){
//            taskDTO.setExecutor(simpleUserMapper.convertToDto(source.getExecutor()));
//        }
//        return taskDTO;
//    }
//
//    public void updateEntityWithDTOData(TaskDTO source, Task destination) {
//        mapper.map(source, destination);
//    }
//
//    public Task createEntityAndUpdateWithDto(TaskDTO source) {
//        Task task = mapper.map(source, Task.class);
//        Set<User> usersToAdd = new HashSet<>();
//
//        User user = userService.findOne(source.getCreator().getId());
//        User user2 = userService.findOne(source.getExecutor().getId());
//        task.setCreator(user);
//        task.setExecutor(user2);
//        return task;
//    }
//
//    public Task updateEntityWithDTOData(TaskDTO taskDto) {
//        //User user = userService.findOne(taskDto.getId());
//        Task task = taskService.findOne(taskDto.getId());
//        SimpleUserDTO creator = taskDto.getCreator();
//        SimpleUserDTO executor = taskDto.getExecutor();
//        User one = userService.findOne(creator.getId());
//        User exec = userService.findOne(executor.getId());
//        task.setCreator(one);
//        task.setExecutor(exec);
//
//        mapper.map(taskDto, task);
//        return task;
//    }
//
//    //
////    public List<TaskDTO> convertToDto(List<Task> source) {
////        List<TaskDTO> taskDTOS= new ArrayList<>();
////
////        for (KudoCardAssign kudoCardAssign : source) {
////            KudoCardAssignDTO kudoCardAssignDTO = convertToDto(kudoCardAssign);
////            kudoCardAssignedDTOList.add(kudoCardAssignDTO);
////        }
////
////
////        return kudoCardAssignedDTOList;
////    }
//
//
//}

package io.github.jhipster.sample.service.mapper;

import io.github.jhipster.sample.domain.Comment;
import io.github.jhipster.sample.domain.Task;
import io.github.jhipster.sample.domain.TaskEstimation;
import io.github.jhipster.sample.domain.TimeSpendedOnTaskByUser;
import io.github.jhipster.sample.repository.Comment.TaskEstimationRepository;
import io.github.jhipster.sample.repository.Comment.TimeSpendedOnTaskByUserRepository;
import io.github.jhipster.sample.service.comment.CommentService;
import io.github.jhipster.sample.service.dto.TaskDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pkarwowski on 13.07.2017.
 */

@Component
public class TaskMapper {

    private ModelMapper mapper;

//    @Autowired
//    private String n;

    @Autowired
    private SimpleUserMapper simpleUserMapper;
    @Autowired
    private TimeSpendedOnTaskByUserRepository timeSpendedOnTaskByUserRepository;

    @Autowired
    private CommentService commentService;
    @Autowired
    private TaskEstimationRepository taskEstimationRepository;

    public TaskMapper() {
    }

    @PostConstruct
    protected void init() {
        mapper = new ModelMapper();
        mapper.getConfiguration().setImplicitMappingEnabled(false);
        mapper.addMappings(new PropertyMap<Task, TaskDTO>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setName(source.getName());
                map().setDescription(source.getDescription());
                map().setTaskStatus(source.getTaskStatus());
                map().setTaskPriority(source.getTaskPriority());
                map().setBeginDate(source.getBeginDate());
                map().setEndDate(source.getEndDate());
                map().setStatus(source.getStatus());
                map().setPriority(source.getPriority());
                map().setSpendedTime(source.getSpendedTime());
                map().setComment(source.getComment());
                map().setEstimation(source.getEstimation());
                map().setAverageEstimation(source.getAverageEstimations());
                map().setTotalSpendedTime(source.getTotalSpendedTime());
                map().setOvertimed(source.getOvertimed());
//                map().setPosts(source.getPosts());
//                map().setTaskEstimations(source.getTaskEstimations());
//                map().setTimeSpendedOnTaskByUser(source.getTimeSpendedOnTaskByUser());

            }
        });
        mapper.addMappings(new PropertyMap<TaskDTO, Task>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setName(source.getName());
                map().setDescription(source.getDescription());
                map().setTaskStatus(source.getTaskStatus());
                map().setTaskPriority(source.getTaskPriority());
                map().setBeginDate(source.getBeginDate());
                map().setEndDate(source.getEndDate());
                map().setStatus(source.getStatus());
                map().setPriority(source.getPriority());
                map().setSpendedTime(source.getSpendedTime());
                map().setComment(source.getComment());
                map().setEstimation(source.getEstimation());
                map().setAverageEstimations(source.getAverageEstimation());
                map().setTotalSpendedTime(source.getTotalSpendedTime());
                map().setOvertimed(source.getOvertimed());
//                map().setPosts(source.getPosts());
//                map().setTaskEstimations(source.getTaskEstimations());
//                map().setTimeSpendedOnTaskByUser(source.getTimeSpendedOnTaskByUser());
            }
        });
    }

    public TaskDTO convertToDto(Task source) {
        TaskDTO dto = mapper.map(source, TaskDTO.class);
        dto.setId(source.getId());
        List<Comment> posts = source.getPosts();
//        dto.setPosts(posts);
        dto.setDescription(source.getDescription());
        dto.setName(source.getName());
//        dto.setTaskStatus(source.getTaskStatus());
//        dto.setTaskPriority(source.getTaskPriority());
        dto.setBeginDate(source.getBeginDate());
        dto.setEndDate(source.getEndDate());
        dto.setSpendedTime(source.getSpendedTime());
        dto.setComment(source.getComment());
        dto.setEstimation(source.getEstimation());
        dto.setAverageEstimation(source.getAverageEstimations());
        dto.setTotalSpendedTime(source.getTotalSpendedTime());
        dto.setOvertimed(source.getOvertimed());
//        dto.setStatus(source.getStatus());

//        dto.setTaskEstimations(source.getTaskEstimations());




        List<TaskEstimation> taskEstimationList = taskEstimationRepository.findAll();
        List<TaskEstimation> taskEstimations = new ArrayList<>();
        Double avg = 0.0;

        if (taskEstimationList != null && !taskEstimationList.isEmpty()) {
            for (int i = 0; i < taskEstimationList.size(); i++) {
                if (taskEstimationList.get(i).getTask().getId().equals(dto.getId())) {
                    taskEstimations.add(taskEstimationList.get(i));
                    avg += taskEstimationList.get(i).getTimeEstimatedByUser();
                }
            }
        }
        dto.setTaskEstimations(taskEstimations);

        List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUserList = timeSpendedOnTaskByUserRepository.findAll();
        List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser = new ArrayList<>();

        if (!(timeSpendedOnTaskByUserList.size()==0)) {
            for (int i = 0; i < timeSpendedOnTaskByUserList.size(); i++) {
                if (timeSpendedOnTaskByUserList.get(i).getTask().getId().equals(source.getId())) {
                    timeSpendedOnTaskByUser.add(timeSpendedOnTaskByUserList.get(i));
                }
            }
            dto.setTimeSpendedOnTaskByUser(timeSpendedOnTaskByUser);
        }


        if (!source.getCreator().equals(null)) {
            dto.setCreator(simpleUserMapper.convertToDto(source.getCreator()));
        }
        List<Comment> comments = commentService.findAll();
        List<Comment> commentsToTask = new ArrayList<>();

        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).getTask().getId().equals(dto.getId())) {
                commentsToTask.add(comments.get(i));
            }
        }
        dto.setPosts(commentsToTask);
        if (!(source.getExecutor()==null)) {

            dto.setExecutor(simpleUserMapper.convertToDto(source.getExecutor()));
        }
        return dto;
    }
    public Task convertFromDto(TaskDTO source) {
        Task dto = mapper.map(source, Task.class);
        dto.setTaskStatus(source.getTaskStatus());
        dto.setTaskPriority(source.getTaskPriority());
        dto.setComment(source.getComment());
        dto.setEstimation(source.getEstimation());
        dto.setAverageEstimations(source.getAverageEstimation());
        dto.setTotalSpendedTime(source.getTotalSpendedTime());
        dto.setOvertimed(source.getOvertimed());

        if(org.springframework.util.ObjectUtils.isEmpty(source.getTimeSpendedOnTaskByUser())) {
            dto.setTimeSpendedOnTaskByUser(source.getTimeSpendedOnTaskByUser());
        }
        if (source.getCreator()!=null) {
            dto.setCreator(simpleUserMapper.convertFromDto(source.getCreator()));
        }
        if (source.getExecutor()!=null) {

            dto.setExecutor(simpleUserMapper.convertFromDto(source.getExecutor()));
        }
        return dto;
    }

    public List<TaskDTO> convertToDto(List<Task> source) {
        List<TaskDTO> dtoList = new ArrayList<>();

        for (Task s : source) {
            dtoList.add(convertToDto(s));
        }

        return dtoList;
    }

}
