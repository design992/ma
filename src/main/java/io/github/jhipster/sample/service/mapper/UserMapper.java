package io.github.jhipster.sample.service.mapper;

import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.dto.ConfirmUserDTO;
import io.github.jhipster.sample.service.dto.NewUserDTO;
import io.github.jhipster.sample.service.dto.UserDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Mapper for the entity User and its DTO called UserDTO.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
//@Service
//public class UserMapper {
//
//    public UserDTO userToUserDTO(User user) {
//        return new UserDTO(user);
//    }
//
//    public List<UserDTO> usersToUserDTOs(List<User> users) {
//        return users.stream()
//            .filter(Objects::nonNull)
//            .map(this::userToUserDTO)
//            .collect(Collectors.toList());
//    }
//
//    public User userDTOToUser(UserDTO userDTO) {
//        if (userDTO == null) {
//            return null;
//        } else {
//            User user = new User();
//            user.setId(userDTO.getId());
//            user.setLogin(userDTO.getLogin());
//            user.setFirstName(userDTO.getFirstName());
//            user.setLastName(userDTO.getLastName());
//            user.setEmail(userDTO.getEmail());
//            user.setImageUrl(userDTO.getImageUrl());
//            user.setActivated(userDTO.isActivated());
//            user.setLangKey(userDTO.getLangKey());
//            user.setNextProject(userDTO.getNextProject());
//            Set<Authority> authorities = this.authoritiesFromStrings(userDTO.getAuthorities());
//            if(authorities != null) {
//                user.setAuthorities(authorities);
//            }
//            return user;
//        }
//    }
//
//    public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
//        return userDTOs.stream()
//            .filter(Objects::nonNull)
//            .map(this::userDTOToUser)
//            .collect(Collectors.toList());
//    }
//
//    public User userFromId(Long id) {
//        if (id == null) {
//            return null;
//        }
//        User user = new User();
//        user.setId(id);
//        return user;
//    }
//
//    public Set<Authority> authoritiesFromStrings(Set<String> strings) {
//        return strings.stream().map(string -> {
//            Authority auth = new Authority();
//            auth.setName(string);
//            return auth;
//        }).collect(Collectors.toSet());
//    }
//}


@Component
public class UserMapper {
    private static final Logger log = LoggerFactory.getLogger(UserMapper.class);
    private ModelMapper mapper;

    @Autowired
    private UserService userService;
    public UserMapper() {
    }
    @PostConstruct
    protected void init() {
        mapper = new ModelMapper();
        mapper.getConfiguration().setImplicitMappingEnabled(false);
        mapper.addMappings(new PropertyMap<User, UserDTO>() {

            /*
             * (non-Javadoc)
             * @see org.modelmapper.PropertyMap#configure()
             */
            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setLogin(source.getLogin());
                map().setFirstName(source.getFirstName());
                map().setLastName(source.getLastName());
                map().setEmail(source.getEmail());
                map().setCertificates(source.getCertificates());
//                map().setPhoneNumber(source.getPhoneNumber());
//                map().setInfo(source.getInfo());
                map().setLangKey(source.getLangKey());
//                map().setVersion(source.getVersion());
                map().setNextProject(source.getNextProject());
//                map().setJobTitle(source.getJobTitle());
            }
        });
        mapper.addMappings(new PropertyMap<UserDTO, User>() {

            @Override
            protected void configure() {
                map().setId(source.getId());
                map().setLogin(source.getLogin());
//                map().setPassword(source.getPassword());
                map().setFirstName(source.getFirstName());
                map().setLastName(source.getLastName());
                map().setEmail(source.getEmail());
                map().setCertificates(source.getCertificates());
//                map().setPhoneNumber(source.getPhoneNumber());
//                map().setInfo(source.getInfo());
                map().setLangKey(source.getLangKey());
//                map().setVersion(source.getVersion());
                map().setNextProject(source.getNextProject());
//                map().setJobTitle(source.getJobTitle());
            }

        });

        mapper.addMappings(new PropertyMap<NewUserDTO, User>() {

            @Override
            protected void configure() {
                map().setFirstName(source.getFirstName());
                map().setLastName(source.getLastName());
                map().setEmail(source.getEmail());
                map().setLangKey(source.getLangKey());
                map().setLogin(source.getLogin());
                map().setCertificates(source.getCertificates());
//                map().setJobTitle(source.getJobTitle());
            }

        });

        mapper.addMappings(new PropertyMap<ConfirmUserDTO, User>() {

            @Override
            protected void configure() {
                map().setLogin(source.getLogin());
                map().setPassword(source.getPassword());
            }
        });

    }
    public UserDTO convertToDto(User source) {
        UserDTO userDTO = mapper.map(source, UserDTO.class);
//        userDTO.setActivated(source.getActivationDate() != null);
//        userDTO.setHasPhoto(source.getImage() != null);
        return userDTO;
    }


    public User updateEntityWithDTOData(UserDTO userDto) {
        User user = userService.findOne(userDto.getId());
//        userDto.setPassword(user.getPassword());
        mapper.map(userDto, user);
        return user;
    }

    public User createEntityAndUpdateWithDto(UserDTO source) {
        return mapper.map(source, User.class);
    }

    public void updateEntityWithDTOData(NewUserDTO source, User destination) {
        mapper.map(source, destination);
    }

    public void updateEntityWithConfirmDTOData(ConfirmUserDTO source, User destination) {
        mapper.map(source, destination);
    }

}
