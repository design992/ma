package io.github.jhipster.sample.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.UrlFromRequestHelper;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.security.SecurityUtils;
import io.github.jhipster.sample.service.ConfigurationService;
import io.github.jhipster.sample.service.FeedbackService;
import io.github.jhipster.sample.service.MailService;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/feedback")
public class FeedbackResource {

    private static final Logger logToFeedbackFile = LoggerFactory.getLogger("feedback-logger");

    @Autowired
    private MailService mailService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private UrlFromRequestHelper urlFromRequestHelper;

    @Autowired
    private FeedbackService feedbackService;

    @RequestMapping(value = "/sendFeedback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MessageDTO> sendFeedback(@RequestBody String feedbackText,
                                                   HttpServletRequest request) throws Exception, OptimisticLockingFailureException {
        // log.debug("REST request to update user, userDTO.id={}", userDTO.getId());
        feedbackService.addNewFeedback(feedbackText);

        String receiverEmail = configurationService.getFeedbackReceiverEmail();
//        User loggedUser = userService.getLoggedUser();
        Optional<User> loggedUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        String baseUrl = urlFromRequestHelper.prepareAppURLBasedOnRequest(request);
        mailService.sendFeedbackEmail(loggedUser.get(), receiverEmail, baseUrl, feedbackText);

        logToFeedbackFile.info("\n[FEEDBACK]\nSent by user: {}\nTo email address: {}\nWith following message:\n\n\"{}\""
            + "----------------------------", loggedUser.get().getLogin(), receiverEmail, feedbackText);

        return new ResponseEntity<>((new MessageDTO("success", MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString())),
            HttpStatus.OK);

    }

}
