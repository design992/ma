package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.Comment;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.CommentService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import io.github.jhipster.sample.web.rest.MessageCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PK on 2017-03-04.
 */
@RestController
@RequestMapping("/api/comment")
public class CommentResource {

    private static final Logger log = LoggerFactory.getLogger(CommentResource.class);

    @Autowired
    private CommentService commentService;


    @Autowired
    private UserService userService;

    //    @PreAuthorize("#skillDTO.getUser().getLogin().equals(authentication.principal.username) or hasRole('"
//            + AuthoritiesConstants.ADMIN + "')")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MessageDTO> addComment(@RequestBody Comment comment) {
        log.debug("---REST request to create comment {}", comment);
        User loggedUser = userService.getLoggedUser();
        comment.setUser(loggedUser);
//        comment.setPostDate(new DateTime());
        commentService.create(comment);

        return new ResponseEntity<>(
                new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
                HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Comment getCommentById(@PathVariable Long id) {
        log.debug("REST request to get Comment : {}", id);
        Comment comment = commentService.findOne(id);
        return comment;
    }
//
//    //    @Scheduled(cron = "* 0 0 9 * * MON-FRI")
//    @Scheduled(cron = "*/40 * * * * *")
//    public void sendMail() {
//        List<User> allUsers = userService.getAllUsers();
//        log.debug("-----logged user: {}  " + allUsers);
//
//    }


    //    @PreAuthorize("@securityService.hasPermissionFindAllSkills(#userId, authentication.principal.companyId)")
    @RequestMapping(value = "/all/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Comment>> findAllCommentsByUserId(@PathVariable Long userId) {
        log.debug("REST request to find all comments of user with id = {}", userId);

        List<Comment> foundCommentList = commentService.findAllByUserId(userId);

        log.debug("--------------\n\nComments:\n {}", foundCommentList.toString());
        return new ResponseEntity<>(foundCommentList, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Comment>> findAllComments() {
        log.debug("REST request to find all comments");

        List<Comment> foundCommentList = commentService.findAll();

        log.debug("--------------\n\nComments:\n {}", foundCommentList.toString());
        return new ResponseEntity<>(foundCommentList, HttpStatus.OK);
    }

    @RequestMapping(value = "/allFromCurrentUserCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Comment>> findAllCommentsFromCurrentUserCompany() {
        log.debug("REST request to find all comments From Current User Company");

        List<Comment> foundCommentList = commentService.findAllCommentsFromLoggedUserCompany();

        log.debug("--------------\n\nComments From Current User Company:\n {}", foundCommentList.toString());
        return new ResponseEntity<>(foundCommentList, HttpStatus.OK);
    }

    @RequestMapping(value = "/allByUserLogin/{login}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Comment>> findAllByUserLogin(@PathVariable String login) {
        log.debug("REST request to find all comments By User Login");

        List<Comment> foundCommentList = commentService.findAllByUserLogin(login);

        log.debug("--------------\n\nComments find All By User Login:\n {}", foundCommentList.toString());
        return new ResponseEntity<>(foundCommentList, HttpStatus.OK);
    }

    @RequestMapping(value = "/allByUser/{user}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Comment>> findAllByUserLogin(@RequestBody User user) {
        log.debug("REST request to find all comments By User");

        List<Comment> foundCommentList = commentService.findAllCommentsByUser(user);

        log.debug("--------------\n\nComments find All By User:\n {}", foundCommentList.toString());
        return new ResponseEntity<>(foundCommentList, HttpStatus.OK);
    }

//    @RequestMapping(value = "/allFromCurrentUserTeams", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<Set<Comment>> findAllCommentsFromCurrentUserTeams() {
//        log.debug("REST request to find all comments From Current User Teams");
//
//        Set<Comment> foundCommentList = commentService.findAllCommentsFromLoggedUserTeams();
//
//        log.debug("--------------\n\nComments From Current User Teams:\n {}", foundCommentList.toString());
//        return new ResponseEntity<>(foundCommentList, HttpStatus.OK);
//    }

    @RequestMapping(value = "/updateComment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MessageDTO> updateComment(@RequestBody Comment comment) {
        commentService.updateCommentData(comment);

        return new ResponseEntity<>(
                (new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString())),
                HttpStatus.OK);

    }
}
