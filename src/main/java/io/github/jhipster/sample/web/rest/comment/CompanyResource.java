package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.Company;
import io.github.jhipster.sample.security.AuthoritiesConstants;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.CompanyService;
import io.github.jhipster.sample.service.dto.CompanyInfoDTO;
import io.github.jhipster.sample.service.mapper.CompanyMapper;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by pkarwowski on 14.07.2017.
 */
@Api(value = "company-info-resource", description = "Pozwala na wykonywanie operacji związanych z firmą użytkownika",
    produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/api")
public class CompanyResource {

    private static final Logger log = LoggerFactory.getLogger(CompanyResource.class);

    @Autowired
    private CompanyService companyInfoService;

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyMapper companyInfoMapper;

    @Secured(AuthoritiesConstants.ADMIN)
    @RequestMapping(value = "/companyInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public CompanyInfoDTO getCompanyInfo() {
        log.debug("REST request to get companyInfo");

        Long userLoggedId = userService.getLoggedUser().getId();
        return companyInfoMapper.convertToDto(companyInfoService.findOneByUserId(userLoggedId));
    }

    @Secured(AuthoritiesConstants.ADMIN)
    @RequestMapping(value = "/companyInfo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> postCompanyInfo(@RequestBody @Valid CompanyInfoDTO companyInfoDTO)
        throws ObjectOptimisticLockingFailureException {
        log.debug("REST request to post CompanyInfoDTO");

        Company newCompany = new Company();
        companyInfoMapper.updateEntityWithDTOData(companyInfoDTO, newCompany);

        Long userLoggedId = userService.getLoggedUser().getId();
        companyInfoService.saveCompanyInfo(newCompany, userLoggedId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
