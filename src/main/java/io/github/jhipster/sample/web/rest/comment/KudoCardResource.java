package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.KudoCard;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.service.MailService;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.KudoCardService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import io.github.jhipster.sample.web.rest.MessageCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

/**
 * Created by PK on 2017-09-03.
 */
@RestController
@RequestMapping("/api")
public class KudoCardResource {
    private final Logger log = LoggerFactory.getLogger(KudoCardResource.class);
    @Autowired
    private UserService userService;
    @Autowired
    private MailService mailService;
    @Autowired
    private KudoCardService kudoCardService;


    @GetMapping("/kudoCardReceived")
    @Timed
    public ResponseEntity<List<KudoCard>> getKudoCardReceivedOfCurrentLoggedUser() {
        log.debug("REST request to find all kudo received");

        List<KudoCard> allKudoCardReceivedByCurrentLoggedUser = kudoCardService.findAllKudoCardReceivedByCurrentLoggedUser();

        log.debug("--------------\n\nallKudoCardReceivedByCurrentLoggedUser:\n {}", allKudoCardReceivedByCurrentLoggedUser.toString());
        return new ResponseEntity<>(allKudoCardReceivedByCurrentLoggedUser, HttpStatus.OK);
    }

    @GetMapping("/kudoCardSent")
    @Timed
    public ResponseEntity<List<KudoCard>> getKudoCardsSentByCurrentLoggedUser() {
        log.debug("REST request to find all kudo sent");

        List<KudoCard> allKudoCardSentByCurrentLoggedUser = kudoCardService.findAllKudoCardSentByCurrentLoggedUser();

        log.debug("--------------\n\nallKudoCardSentByCurrentLoggedUser:\n {}", allKudoCardSentByCurrentLoggedUser.toString());
        return new ResponseEntity<>(allKudoCardSentByCurrentLoggedUser, HttpStatus.OK);
    }

    @GetMapping("/kudoCard")
    @Timed
    public ResponseEntity<List<KudoCard>> getKudoCardsSentByCurrentLoggedUserFront() {
        log.debug("REST request to find all kudo sent");

        List<KudoCard> allKudoCardSentByCurrentLoggedUser = kudoCardService.findAllKudoCardSentByCurrentLoggedUser();

        log.debug("--------------\n\nallKudoCardSentByCurrentLoggedUser:\n {}", allKudoCardSentByCurrentLoggedUser.toString());
        return new ResponseEntity<>(allKudoCardSentByCurrentLoggedUser, HttpStatus.OK);
    }

    @DeleteMapping("/kudoCard/{id}")
    @Timed
    public ResponseEntity<MessageDTO> deleteKudoCard(@PathVariable Long id) {
        log.debug("REST request to delete kudoCard : {}", id);
        kudoCardService.delete(id);
        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.OK);
    }

    @PostMapping("/kudoCard")
    @Timed
    public ResponseEntity<MessageDTO> addKudoCard(@RequestBody KudoCard kudoCard) throws MessagingException {
        log.debug("REST request to create kudoCard");
        User loggedUser = userService.getLoggedUser();
        kudoCard.setFromUser(loggedUser);
        kudoCard.setToUser(loggedUser);
        kudoCardService.save(kudoCard);
        mailService.sendKudoEmail(loggedUser,loggedUser,"http://localhost:8080");
        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.CREATED);
    }

    @GetMapping("/kudoCardAll")
    @Timed
    public ResponseEntity<List<KudoCard>> getKudoCards() {
        log.debug("REST request to find all kudos");

        List<KudoCard> kudoCards = kudoCardService.findAll();

        log.debug("--------------\n\nkudoCards:\n {}", kudoCards.toString());
        return new ResponseEntity<>(kudoCards, HttpStatus.OK);
    }
}
