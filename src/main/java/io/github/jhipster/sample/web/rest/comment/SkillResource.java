package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.Skill;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.domain.UserSkill;
import io.github.jhipster.sample.repository.Comment.SkillRepository;
import io.github.jhipster.sample.repository.UserRepository;
import io.github.jhipster.sample.service.MailService;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.SkillService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import io.github.jhipster.sample.service.dto.SimpleUserDTO;
import io.github.jhipster.sample.service.dto.SkillDTO;
import io.github.jhipster.sample.service.mapper.SimpleUserMapper;
import io.github.jhipster.sample.service.mapper.SkillMapper;
import io.github.jhipster.sample.web.rest.MessageCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SkillResource {

    private final Logger log = LoggerFactory.getLogger(SkillResource.class);

    @Autowired
    private MailService mailService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SkillService skillService;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private SkillMapper skillMapper;
    @Autowired
    private SimpleUserMapper simpleUserMapper;
    @Autowired
    private UserService userService;
    //    @RequestMapping(value = "/skillsOfLoggedUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public List<Skill> getAllSkillsOfCurrentLoggedUser() {
//        log.debug("REST request to get all skills of current logged user");
//        return skillService.findAllSkillsOfCurrentLoggedUser();
//    }
//
//    @RequestMapping(value = "/addSkill", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<MessageDTO> addSkill(@RequestBody SkillDTO skillDTO) {
//        log.debug("REST request to create task");
//
//        User loggedUser = userService.getLoggedUser();
//        int size = skillService.findAll().size() + 1;
//        List<Skill> skills = skillRepository.findAll();
//
//        Skill skill = skillMapper.convertFromDto(skillDTO);
//        skill.setUser(loggedUser);
//        skillService.create(skill);
//
//        return new ResponseEntity<>(
//            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
//            HttpStatus.CREATED);
//    }
//
//    @RequestMapping(value = "/skill/all/skillById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<SkillDTO> getSkillWithId(@PathVariable Long id) {
//        log.debug("REST request to get skill with id : {}", id);
//        Skill skill = skillService.findOne(id);
//
//        SkillDTO skillDTO = skillMapper.convertToDto(skill);
//        return new ResponseEntity<>(skillDTO, HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/skill/all/skillByUserId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<List<SkillDTO>> findAllSkillsOfUSerById(@PathVariable Long id) {
//        log.debug("REST request to  findAllSkillsOfUSerByUserId with user id : {}", id);
//        User user = userService.findOne(id);
//        List<Skill> skillsOfUser = new ArrayList<>();
//        List<Skill> skills = skillService.findAll();
//
//        for (int i = 0; i < skills.size(); i++) {
//            if (skills.get(i).getUser().getId().equals(user.getId())) {
//                skillsOfUser.add(skills.get(i));
//            }
//        }
//
//        List<SkillDTO> skillDTO = skillMapper.convertToDto(skillsOfUser);
//        return new ResponseEntity<>(skillDTO, HttpStatus.OK);
//    }
//    @RequestMapping(value = "/skill/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<List<SkillDTO>> getAllSkills() {
//        log.debug("REST request to  all skills");
//        List<Skill> skills = skillService.findAll();
//        List<SkillDTO> skillDTO = skillMapper.convertToDto(skills);
//        return new ResponseEntity<>(skillDTO, HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/skill/deleteSkill", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<MessageDTO> deleteSkill(@RequestBody SkillDTO skillDTO) {
//        log.debug("REST request to save task");
//        log.debug("task id : " + skillDTO.getId());
//        Skill skill = skillService.findOne(skillDTO.getId());
//
//        skillService.delete(skill);
//        return new ResponseEntity<>(
//            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
//            HttpStatus.OK);
//    }
    @PostMapping("/skills")
    @Timed
    public ResponseEntity<MessageDTO> addSkill(@RequestBody Skill skill) {
        log.debug("REST request to create task");

        User loggedUser = userService.getLoggedUser();
        skill.setUser(loggedUser);
        skillService.create(skill);

        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.CREATED);
    }

    @GetMapping("/skills")
    @Timed
    public ResponseEntity<List<Skill>> getAllSkills() {
        log.debug("REST request to find all skills");

        List<Skill> skills = skillService.findAll();

        log.debug("--------------\n\nskills:\n {}", skills.toString());
        return new ResponseEntity<>(skills, HttpStatus.OK);
    }

    @GetMapping("/skills/{id}")
    @Timed
    public ResponseEntity<SkillDTO> getSkillWithId(@PathVariable Long id) {
        log.debug("REST request to get Skill with id : {}", id);

        Skill skill = skillService.findOne(id);
        SkillDTO skillDTO = skillMapper.convertToDto(skill);
        return new ResponseEntity<>(skillDTO, HttpStatus.OK);
    }

    @DeleteMapping("/skills/{id}")
    @Timed
    public ResponseEntity<MessageDTO> deleteSkill(@PathVariable Long id) {
        log.debug("REST request to delete Skill : {}", id);
        skillRepository.delete(id);
        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.OK);
    }

    @PutMapping("/skills")
    @Timed
    public ResponseEntity<MessageDTO> editSkill(@RequestBody SkillDTO skillDTO) {
        log.debug("REST request to save skill");

        User loggedUser = userService.getLoggedUser();
        skillDTO.setUser(simpleUserMapper.convertToDto(loggedUser));
        Skill skill = skillMapper.convertFromDto(skillDTO);
        skillService.save(skill);

        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.OK);
    }


}
