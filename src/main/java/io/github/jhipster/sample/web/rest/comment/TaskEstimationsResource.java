package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.TaskEstimation;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.TaskEstimationRepository;
import io.github.jhipster.sample.repository.Comment.TaskPriorityRepository;
import io.github.jhipster.sample.repository.Comment.TaskRepository;
import io.github.jhipster.sample.repository.Comment.TaskStatusRepository;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.CommentService;
import io.github.jhipster.sample.service.comment.TaskEstimationService;
import io.github.jhipster.sample.service.comment.TaskService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import io.github.jhipster.sample.service.mapper.SimpleUserMapper;
import io.github.jhipster.sample.service.mapper.TaskMapper;
import io.github.jhipster.sample.web.rest.MessageCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by PK on 2017-03-04.
 */
@RestController
@RequestMapping("/api/tasksEstimations")
public class TaskEstimationsResource {

    private static final Logger log = LoggerFactory.getLogger(TaskEstimationsResource.class);

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskPriorityRepository taskPriorityRepository;
    @Autowired
    private TaskEstimationRepository taskEstimationRepository;

    @Autowired
    private TaskStatusRepository taskStatusRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private CommentService commentService;
    @Autowired
    private TaskEstimationService taskEstimationService;

    @Autowired
    private SimpleUserMapper simpleUserMapper;


    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Boolean> xsltGenerator() {
        List<TaskEstimation> all = taskEstimationRepository.findAll();
        log.debug("ok: "+all);
        return new ResponseEntity<>( Boolean.TRUE, HttpStatus.OK);
    }


    @RequestMapping(value = "/add", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MessageDTO> add(@RequestBody TaskEstimation taskEstimation) {
        log.debug("REST request to create task");

        User loggedUser = userService.getLoggedUser();
        taskEstimation.setUser(loggedUser);
        List<TaskEstimation> all = taskEstimationRepository.findAll();
        Optional<TaskEstimation> taskEstimationFinded = taskEstimationRepository.findAllByUserAndTask(loggedUser, taskEstimation.getTask());
        if (!taskEstimationFinded.isPresent()) {
            taskEstimationService.add(taskEstimation);
        } else {
//            TaskEstimation taskEstimation1 = taskEstimationFinded.get();
            TaskEstimation taskEstimation1 = taskEstimationRepository.findOne(taskEstimationFinded.get().getId());
            taskEstimation1.setTimeEstimatedByUser(taskEstimation.getTimeEstimatedByUser());
//            taskEstimationService.add(taskEstimation1);
            taskEstimationRepository.saveAndFlush(taskEstimation1);
        }
        return new ResponseEntity<>(
                new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
                HttpStatus.CREATED);
    }


}
