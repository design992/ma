package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.*;
import io.github.jhipster.sample.repository.Comment.*;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.CommentService;
import io.github.jhipster.sample.service.comment.SkillService;
import io.github.jhipster.sample.service.comment.TaskEstimationService;
import io.github.jhipster.sample.service.comment.TaskService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import io.github.jhipster.sample.service.dto.SimpleUserDTO;
import io.github.jhipster.sample.service.dto.TaskDTO;
import io.github.jhipster.sample.service.mapper.SimpleUserMapper;
import io.github.jhipster.sample.service.mapper.SkillMapper;
import io.github.jhipster.sample.service.mapper.TaskMapper;
import io.github.jhipster.sample.web.rest.MessageCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by PK on 2017-03-04.
 */
@RestController
@RequestMapping("/api")
public class TaskResource {

    private static final Logger log = LoggerFactory.getLogger(TaskResource.class);

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskPriorityRepository taskPriorityRepository;
    @Autowired
    private TaskEstimationRepository taskEstimationRepository;

    @Autowired
    private TaskStatusRepository taskStatusRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

//    @Autowired
//    private TextNormalizer textNormalizer;

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private CommentService commentService;
    @Autowired
    private TaskEstimationService taskEstimationService;
    @Autowired
    private TimeSpendedOnTaskByUserRepository timeSpendedOnTaskByUserRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private SimpleUserMapper simpleUserMapper;
    @Autowired
    private SkillMapper skillMapper;
    @Autowired
    private SkillService skillService;
//    private static TaskEstimation taskEstimation = new TaskEstimation();


    @RequestMapping(value = "/xmlGenerator", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @GetMapping("/xsltGenerator")
    @Timed
    public ResponseEntity<List<Task>> xmlGenerator() {
        taskService.writeXmlFile();
        log.debug("CREATEDOK");

        List<Task> taskList = taskService.findAllTasksOfCurrentLoggedUserCompany();

        log.debug("--------------\n\ntaks:\n {}", taskList.toString());
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

//    @RequestMapping(value = "/getCountOfTasksCreatedByUsersFromCurrentUserCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<List<UserWithCountOfTasks>> getCountOfTasksCreatedByUsersFromCurrentUserCompany() {
//        List<Task> taskList = taskService.findAll();
//        List<User> activeUsersFromLoggedUserCompany = userService.getActiveUsersFromLoggedUserCompany();
//        List<UserWithCountOfTasks> listOfTasksCreatedByUser = new ArrayList<>();
//        for (User user:activeUsersFromLoggedUserCompany) {
//            SimpleUserDTO simpleUserDTO = simpleUserMapper.convertToDto(user);
//            UserWithCountOfTasks userWithCountOfTasks = new UserWithCountOfTasks();
//            userWithCountOfTasks.setSimpleUserDTO(simpleUserDTO);
//            int count=0;
//            for (Task task : taskList) {
//                if (task.getCreator().equals(user)){
//                    count++;
//                }
//            }
//            userWithCountOfTasks.setCount(count);
//            listOfTasksCreatedByUser.add(userWithCountOfTasks);
//        }
//        log.debug("listOfTasksCreatedByUser:"+listOfTasksCreatedByUser);
//        return new ResponseEntity<>(listOfTasksCreatedByUser, HttpStatus.OK);
//    }


    //    @RequestMapping(value = "/getCountOfTasksAssignedToUsersFromCurrentUserCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/getCountOfTasksAssignedToUsersFromCurrentUserCompany")
    @Timed
    public ResponseEntity<List<UserWithCountOfTasks>> getCountOfTasksAssignedToUsersFromCurrentUserCompany() {
        List<Task> taskList = taskService.findAll();
        List<User> activeUsersFromLoggedUserCompany = userService.getActiveUsersFromLoggedUserCompany();
        List<UserWithCountOfTasks> listOfTasksCreatedByUser = new ArrayList<>();
        for (User user : activeUsersFromLoggedUserCompany) {
            SimpleUserDTO simpleUserDTO = simpleUserMapper.convertToDto(user);
            UserWithCountOfTasks userWithCountOfTasks = new UserWithCountOfTasks();
            userWithCountOfTasks.setSimpleUserDTO(simpleUserDTO);
            int count = 0;
            for (Task task : taskList) {
                if (task.getExecutor().equals(user)) {
                    count++;
                }
            }
            userWithCountOfTasks.setKount(count);
            listOfTasksCreatedByUser.add(userWithCountOfTasks);
        }
        log.debug("getCountOfTasksAssignedToUsersFromCurrentUserCompany:" + listOfTasksCreatedByUser);
        return new ResponseEntity<>(listOfTasksCreatedByUser, HttpStatus.OK);
    }

    @GetMapping("/getTaskStatusesWithCount")
    @Timed
    public ResponseEntity<List<TaskStatusWithCount>> getTaskStatusesWithCount() {
        List<TaskStatus> taskStatuses = taskStatusRepository.findAll();
        List<TaskStatusWithCount> listOfTaskStatusesWithCount = new ArrayList<>();
        List<Task> taskList = taskService.findAllTasksOfCurrentLoggedUserCompany();

        for (TaskStatus taskStatus : taskStatuses) {
            int count = 0;
            for (Task task : taskList) {
                if (task.getTaskStatus().getName().equals(taskStatus.getName())) {
                    count++;
                }
            }
            TaskStatusWithCount taskStatusWithCount = new TaskStatusWithCount();
            taskStatusWithCount.setCount(count);
            taskStatusWithCount.setTaskStatus(taskStatus);
            listOfTaskStatusesWithCount.add(taskStatusWithCount);
        }
        log.debug("listOfTaskStatusesWithCount:" + listOfTaskStatusesWithCount);
        Comparator<TaskStatusWithCount> byCount = (e1, e2) -> Integer.compare(
            e1.getCount(), e2.getCount());
        listOfTaskStatusesWithCount = listOfTaskStatusesWithCount.stream().sorted(byCount.reversed()).collect(Collectors.toList());

        return new ResponseEntity<>(listOfTaskStatusesWithCount, HttpStatus.OK);
    }

    @GetMapping("/getTaskPrioritiesWithCount")
    @Timed
    public ResponseEntity<List<TaskPriorityWithCount>> getTaskPrioritiesWithCount() {
        List<TaskPriority> taskPriorities = taskPriorityRepository.findAll();
        List<TaskPriorityWithCount> listOfTaskPrioritiesWithCount = new ArrayList<>();
        List<Task> taskList = taskService.findAllTasksOfCurrentLoggedUserCompany();

        for (TaskPriority taskPriority : taskPriorities) {
            int count = 0;
            for (Task task : taskList) {
                if (task.getTaskPriority().getName().equals(taskPriority.getName())) {
                    count++;
                }
            }
            TaskPriorityWithCount taskPriorityWithCount = new TaskPriorityWithCount();
            taskPriorityWithCount.setCount(count);
            taskPriorityWithCount.setTaskPriority(taskPriority);
            listOfTaskPrioritiesWithCount.add(taskPriorityWithCount);
        }
        log.debug("listOfTaskPrioritiesWithCount:" + listOfTaskPrioritiesWithCount);
        Comparator<TaskPriorityWithCount> byCount = (e1, e2) -> Integer.compare(
            e1.getCount(), e2.getCount());
        listOfTaskPrioritiesWithCount = listOfTaskPrioritiesWithCount.stream().sorted(byCount.reversed()).collect(Collectors.toList());

        return new ResponseEntity<>(listOfTaskPrioritiesWithCount, HttpStatus.OK);
    }

    @GetMapping("/getCountOfTasksCreatedByUsersFromCurrentUserCompany")
    @Timed
    public ResponseEntity<List<UserWithCountOfTasks>> getCountOfTasksCreatedByUsersFromCurrentUserCompany() {
        List<Task> taskList = taskService.findAll();
        List<User> activeUsersFromLoggedUserCompany = userService.getActiveUsersFromLoggedUserCompany();
        List<UserWithCountOfTasks> listOfTasksCreatedByUser = new ArrayList<>();
        for (User user : activeUsersFromLoggedUserCompany) {
            SimpleUserDTO simpleUserDTO = simpleUserMapper.convertToDto(user);
            UserWithCountOfTasks userWithCountOfTasks = new UserWithCountOfTasks();
            userWithCountOfTasks.setSimpleUserDTO(simpleUserDTO);
            int count = 0;
            for (Task task : taskList) {
                if (task.getCreator().equals(user)) {
                    count++;
                }
            }
            userWithCountOfTasks.setKount(count);
            listOfTasksCreatedByUser.add(userWithCountOfTasks);
        }
        log.debug("getCountOfTasksCreatedByUsersFromCurrentUserCompany:" + listOfTasksCreatedByUser);
        return new ResponseEntity<>(listOfTasksCreatedByUser, HttpStatus.OK);
    }

    @GetMapping("/tasks")
    @Timed
    public ResponseEntity<List<Task>> getAllTasks() {
        log.debug("REST request to find all comments");

        List<Task> taskList = taskService.findAllTasksOfCurrentLoggedUserCompany();

        log.debug("--------------\n\ntaks:\n {}", taskList.toString());
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

    @GetMapping("/tasks/{id}")
    @Timed
    public ResponseEntity<TaskDTO> getTaskWithId(@PathVariable Long id) {
        log.debug("REST request to get Task with id : {}", id);

        Task task = taskService.findOne(id);
        TaskStatus status = taskStatusRepository.findByName(task.getStatus());
        TaskPriority priority = taskPriorityRepository.findByName(task.getPriority());
//        task.setTaskStatus(status);
//        task.setTaskPriority(priority);

        List<TaskEstimation> taskEstimationList = taskEstimationRepository.findAll();
        List<TaskEstimation> taskEstimations = new ArrayList<>();
        Double avg = 0.0;

        if (taskEstimationList != null && !taskEstimationList.isEmpty()) {
            for (int i = 0; i < taskEstimationList.size(); i++) {
                if (taskEstimationList.get(i).getTask().getId().equals(id)) {
                    taskEstimations.add(taskEstimationList.get(i));
                    avg += taskEstimationList.get(i).getTimeEstimatedByUser();
                }
            }
        }
        task.setTaskEstimations(taskEstimations);
        task.setAverageEstimations(avg);

        // time spended on task

        List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUserList = timeSpendedOnTaskByUserRepository.findAll();
        List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser = new ArrayList<>();


        if (timeSpendedOnTaskByUserList != null) {
            for (int i = 0; i < timeSpendedOnTaskByUserList.size(); i++) {
                if (timeSpendedOnTaskByUserList.get(i).getTask().getId().equals(id)) {
                    timeSpendedOnTaskByUser.add(timeSpendedOnTaskByUserList.get(i));
                }
            }
            task.setTimeSpendedOnTaskByUser(timeSpendedOnTaskByUser);
        }


        // get all time spended on task
        timeSpendedOnTaskByUserList = timeSpendedOnTaskByUserRepository.findAll();
        if (timeSpendedOnTaskByUserList != null) {
            List<Task> tasks = taskService.findAll();
            Double totalSpendedTime = 0.0;
            for (int i = 0; i < timeSpendedOnTaskByUserList.size(); i++) {
                if (timeSpendedOnTaskByUserList.get(i).getTask().getId().equals(id)) {
                    if (timeSpendedOnTaskByUserList.get(i).getTimeEstimatedByUser() != null) {
                        totalSpendedTime += timeSpendedOnTaskByUserList.get(i).getTimeEstimatedByUser();
                    }
                }
            }
            task.setTotalSpendedTime(totalSpendedTime);
        }

        List<Comment> comments = commentService.findAll();
        List<Comment> commentsToTask = new ArrayList<>();

        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).getTask().getId().equals(id)) {
                commentsToTask.add(comments.get(i));
            }
        }
        task.setPosts(commentsToTask);

        TaskDTO taskDTO = taskMapper.convertToDto(task);
        return new ResponseEntity<>(taskDTO, HttpStatus.OK);
    }

    @DeleteMapping("/tasks/{id}")
    @Timed
    public ResponseEntity<MessageDTO> deleteTask(@PathVariable Long id) {
        log.debug("REST request to delete Task : {}", id);
        taskRepository.delete(id);
        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.OK);
    }

    //    @RequestMapping(value = "/editTaskPost", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PutMapping("/tasks")
    @Timed
    public ResponseEntity<MessageDTO> editTask(@RequestBody TaskDTO taskDTO) {
        log.debug("REST request to save task");

        User loggedUser = userService.getLoggedUser();

/*        String idOfTaskPriority = taskDTO.getTaskPriority().getName();
        TaskPriority taskPriority = taskPriorityRepository.findOne(Long.valueOf(idOfTaskPriority));
        String priorityName = taskPriority.getName();
        taskDTO.setPriority(priorityName);
        String idOfTaskStatus = taskDTO.getTaskStatus().getName();
        TaskStatus taskstatus = taskStatusRepository.findOne(Long.valueOf(idOfTaskStatus));
        taskDTO.setStatus(taskstatus.getName());*/

//        task.setStatus("CREATED");

        taskDTO.setCreatorId(loggedUser.getId());

        Task task = taskMapper.convertFromDto(taskDTO);
        TimeSpendedOnTaskByUser timeSpendedOnTaskByUser = new TimeSpendedOnTaskByUser();
        timeSpendedOnTaskByUser.setTimeEstimatedByUser(task.getSpendedTime());
        timeSpendedOnTaskByUser.setUser(userService.getLoggedUser());
        timeSpendedOnTaskByUser.setTask(taskService.findOne(task.getId()));
        timeSpendedOnTaskByUserRepository.save(timeSpendedOnTaskByUser);

        List<TimeSpendedOnTaskByUser> timeSpendedOnTaskByUser1 = task.getTimeSpendedOnTaskByUser();
        if (timeSpendedOnTaskByUser1 == null) {
            List<TimeSpendedOnTaskByUser> list = new ArrayList<>();
            list.add(timeSpendedOnTaskByUser);
            task.setTimeSpendedOnTaskByUser(list);
        } else {
            timeSpendedOnTaskByUser1.add(timeSpendedOnTaskByUser);
        }

//        addComment(task);
        if (!(task.getComment() == null)) {
            Comment comment = new Comment();
            comment.setUser(userService.getLoggedUser());
            comment.setTask(task);
            comment.setContent(task.getComment());
            commentRepository.save(comment);

        }
        List<Comment> comments = commentRepository.findAll();
        if (comments != null) {
            List<Comment> commentsToTask = new ArrayList<>();
            for (int i = 0; i < comments.size(); i++) {
                if (comments.get(i).getTask().getId().equals(task.getId())) {
                    commentsToTask.add(comments.get(i));
                }
            }
            task.setPosts(commentsToTask);
        }
//        estimateTask(loggedUser, task);

        if (!(task.getEstimation() == null)) {
            TaskEstimation taskEstimation = new TaskEstimation();
            taskEstimation.setUser(loggedUser);
            taskEstimation.setTask(task);
            taskEstimation.setTimeEstimatedByUser(task.getEstimation());
//            List<TaskEstimation> all = taskEstimationRepository.findAll();

            Optional<TaskEstimation> taskEstimationFinded = taskEstimationRepository.findAllByUserAndTask(loggedUser, taskEstimation.getTask());
            if (!taskEstimationFinded.isPresent()) {
                taskEstimationService.add(taskEstimation);
            } else {
                TaskEstimation taskEstimation1 = taskEstimationRepository.findOne(taskEstimationFinded.get().getId());
                taskEstimation1.setTimeEstimatedByUser(taskEstimation.getTimeEstimatedByUser());
                taskEstimationRepository.saveAndFlush(taskEstimation1);
            }
        }
        List<TaskEstimation> taskEstimationRepositoryAll = taskEstimationRepository.findAll();
        if (taskEstimationRepositoryAll != null) {
            task.setTaskEstimations(taskEstimationRepositoryAll);
        }
        taskService.save(task);
        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.OK);
    }

    private void addComment(Task task) {
        if (!(task.getComment() == null)) {
            Comment comment = new Comment();
            comment.setUser(userService.getLoggedUser());
            comment.setTask(task);
            comment.setContent(task.getComment());
            Instant now = Instant.now();
//            Instant instant = Instant.now();
//            comment.setPostDate(now);
            commentRepository.save(comment);
        }
    }

    private void estimateTask(User loggedUser, Task task) {
        if (!(task.getEstimation() == null)) {
            TaskEstimation taskEstimation = new TaskEstimation();
            taskEstimation.setUser(loggedUser);
            taskEstimation.setTask(task);
            taskEstimation.setTimeEstimatedByUser(task.getEstimation());
//            List<TaskEstimation> all = taskEstimationRepository.findAll();

            Optional<TaskEstimation> taskEstimationFinded = taskEstimationRepository.findAllByUserAndTask(loggedUser, taskEstimation.getTask());
            if (!taskEstimationFinded.isPresent()) {
                taskEstimationService.add(taskEstimation);
            } else {
                TaskEstimation taskEstimation1 = taskEstimationRepository.findOne(taskEstimationFinded.get().getId());
                taskEstimation1.setTimeEstimatedByUser(taskEstimation.getTimeEstimatedByUser());
                taskEstimationRepository.saveAndFlush(taskEstimation1);
            }
        }
    }

    //    @RequestMapping(value = "/allTasksOfCurrentLoggedUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/allTasksOfCurrentLoggedUser")
    @Timed
    public ResponseEntity<List<Task>> getAllTasksOfCurrentLoggedUser() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUser : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUser();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    //    @RequestMapping(value = "/findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh")
    @Timed
    public ResponseEntity<List<Task>> getAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    //    @RequestMapping(value = "/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow")
    @Timed
    public ResponseEntity<List<TaskDTO>> getAllTasksOfCurrentLoggedUserCreatorAndPriorityLow() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(allTasksOfCurrentLoggedUser);
        return new ResponseEntity<>(taskDTOS, HttpStatus.OK);
    }

    //    @RequestMapping(value = "/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal")
    @Timed
    public ResponseEntity<List<TaskDTO>> getAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(allTasksOfCurrentLoggedUser);
        return new ResponseEntity<>(taskDTOS, HttpStatus.OK);
    }

    @GetMapping("/findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh")
    @Timed
    public ResponseEntity<List<Task>> getAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    //    @RequestMapping(value = "/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow")
    @Timed
    public ResponseEntity<List<TaskDTO>> getAllTasksOfCurrentLoggedUserExecutorAndPriorityLow() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(allTasksOfCurrentLoggedUser);
        return new ResponseEntity<>(taskDTOS, HttpStatus.OK);
    }

    //    @RequestMapping(value = "/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal")
    @Timed
    public ResponseEntity<List<TaskDTO>> getAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(allTasksOfCurrentLoggedUser);
        return new ResponseEntity<>(taskDTOS, HttpStatus.OK);
    }


    //    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("/tasks")
    @Timed
    public ResponseEntity<MessageDTO> addTask(@RequestBody TaskDTO taskDTO) {
        log.debug("REST request to create task");


        User loggedUser = userService.getLoggedUser();

/*        String idOfTaskPriority = taskDTO.getTaskPriority().getName();
        TaskPriority taskPriority = taskPriorityRepository.findOne(Long.valueOf(idOfTaskPriority));
        String priorityName = taskPriority.getName();
        taskDTO.setPriority(priorityName);
        String idOfTaskStatus = taskDTO.getTaskStatus().getName();
        TaskStatus taskstatus = taskStatusRepository.findOne(Long.valueOf(idOfTaskStatus));
        taskDTO.setStatus(taskstatus.getName());*/

//        task.setStatus("CREATED");
        taskDTO.setCreatorId(loggedUser.getId());
        taskDTO.setCreator(simpleUserMapper.convertToDto(loggedUser));
//        taskDTO.setTaskPriority(taskPriorityRepository.findOne(1l));

        Task task = taskMapper.convertFromDto(taskDTO);
        taskService.save(task);


        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.CREATED);
    }

    @RequestMapping(value = "/taskPriorities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TaskPriority> getAllPriorities() {
        log.debug("REST request to get list of task priorities");
//        return Stream.of(TaskPriority.values()).map(TaskPriority::name).toArray(String[]::new);
        List<TaskPriority> taskPriorityList = taskPriorityRepository.findAll();
        return taskPriorityList;
    }

    @RequestMapping(value = "/taskStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<TaskStatus> getAllStatuses() {
        log.debug("REST request to get list of task statuses");
        return taskStatusRepository.findAll();
    }

    //    @RequestMapping(value = "/addTaskEstimation", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("/addTaskEstimation")
    @Timed
    public ResponseEntity<MessageDTO> add(@RequestBody Double taskEstimation) {
        log.debug("REST request to create task");

//
//        User loggedUser = userService.getLoggedUser();
//        taskEstimation.setUser(loggedUser);
//        List<TaskEstimation> all = taskEstimationRepository.findAll();
//        Optional<TaskEstimation> taskEstimationFinded = taskEstimationRepository.findAllByUserAndTask(loggedUser, taskEstimation.getTask());
//        if (!taskEstimationFinded.isPresent()) {
//            taskEstimationService.add(taskEstimation);
//        } else {
//            TaskEstimation taskEstimation1 = taskEstimationRepository.findOne(taskEstimationFinded.get().getId());
//            taskEstimation1.setTimeEstimatedByUser(taskEstimation.getTimeEstimatedByUser());
//            taskEstimationRepository.saveAndFlush(taskEstimation1);
//        }
        return new ResponseEntity<>(
            new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
            HttpStatus.CREATED);
    }

    @GetMapping("/estimationSample")
    @Timed
    public TaskEstimation getSampleTaskEstimation() {
        TaskEstimation taskEstimation = new TaskEstimation();
        taskEstimation.setUser(userService.getLoggedUser());
        taskEstimation.setTask(taskService.findOne(1l));
        taskEstimation.setTimeEstimatedByUser(12.0);
        return taskEstimation;
    }

    @GetMapping("/tasksBeginToday")
    @Timed
    public ResponseEntity<List<TaskDTO>> getTasksWhichShouldStartToday() {
        log.debug("REST request to getTasksWhichShouldStartToday");

        List<Task> tasks = taskService.findAll();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(tasks);
        List<TaskDTO> tasksWhichStartsToday = new ArrayList<>();
        Instant now = Instant.now();
        for (int i = 0; i < taskDTOS.size(); i++) {
            if (taskDTOS.get(i).getBeginDate().truncatedTo(ChronoUnit.DAYS).equals(now.truncatedTo(ChronoUnit.DAYS))) {
                tasksWhichStartsToday.add(taskDTOS.get(i));
            }

        }

        return new ResponseEntity<>(tasksWhichStartsToday, HttpStatus.OK);
    }

    @GetMapping("/tasksToEstimate")
    public ResponseEntity<List<TaskDTO>> getTasksToEstimateOfCurrentLoggedUser() {
        List<Task> allTasksOfCurrentLoggedUserCompany = taskService.findAllTasksOfCurrentLoggedUserCompany();
        List<TaskDTO> tasksToEstimate = new ArrayList<>();
        Long id = userService.getLoggedUser().getId();
        List<TaskDTO> taskDTOList = taskMapper.convertToDto(allTasksOfCurrentLoggedUserCompany);
        for (TaskDTO t : taskDTOList) {
            if (null != t.getTaskEstimations()) {
                boolean contains = false;
                for (TaskEstimation estimation : t.getTaskEstimations()) {
                    if (estimation.getUser().getId().equals(id)) {
                        contains = true;
                    }
                }
                if (contains == false) {
                    tasksToEstimate.add(t);
                }
            } else {
                tasksToEstimate.add(t);
            }
        }
        return new ResponseEntity<>(tasksToEstimate, HttpStatus.OK);
    }

    @GetMapping("/tasksEstimated")
    public ResponseEntity<List<TaskDTO>> getTasksEstimatedByCurrentLoggedUser() {
        List<Task> allTasksOfCurrentLoggedUserCompany = taskService.findAllTasksOfCurrentLoggedUserCompany();
        List<TaskDTO> tasksEstimated = new ArrayList<>();
        Long id = userService.getLoggedUser().getId();
        List<TaskDTO> taskDTOList = taskMapper.convertToDto(allTasksOfCurrentLoggedUserCompany);
        for (TaskDTO t : taskDTOList) {
            if (null != t.getTaskEstimations()) {
                boolean contains = false;
                for (TaskEstimation estimation : t.getTaskEstimations()) {
                    if (estimation.getUser().getId().equals(id)) {
                        contains = true;
                    }
                }
                if (contains == true) {
                    tasksEstimated.add(t);
                }
            }
        }
        return new ResponseEntity<>(tasksEstimated, HttpStatus.OK);
    }

    @GetMapping("/tasksEndsToday")
    @Timed
    public ResponseEntity<List<TaskDTO>> getTasksWhichShouldEndsToday() {
        log.debug("REST request to getTasksWhichShouldEndsToday");

        List<Task> tasks = taskService.findAll();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(tasks);
        List<TaskDTO> tasksWhichStartsToday = new ArrayList<>();
        Instant now = Instant.now();
        for (int i = 0; i < taskDTOS.size(); i++) {
            if (taskDTOS.get(i).getEndDate().truncatedTo(ChronoUnit.DAYS).equals(now.truncatedTo(ChronoUnit.DAYS))) {
                tasksWhichStartsToday.add(taskDTOS.get(i));
            }
        }

        return new ResponseEntity<>(tasksWhichStartsToday, HttpStatus.OK);
    }

    @GetMapping("/tasksOvertimed")
    @Timed
    public ResponseEntity<List<TaskDTO>> getTasksOvertimed() {
        log.debug("REST request to getTasksOvertimed");

        List<Task> tasks = taskService.findAllTasksOfCurrentLoggedUserCompany();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(tasks);
        List<TaskDTO> tasksOvertimed = new ArrayList<>();
        for (TaskDTO dto : taskDTOS) {
            if (dto.getTaskEstimations().size() > 0) {
                double sum = 0.0;
                for (TaskEstimation e : dto.getTaskEstimations()) {
                    sum += e.getTimeEstimatedByUser();
                }
                double avg = sum / dto.getTaskEstimations().size();
                dto.setAverageEstimation(avg);
            }
            if (dto.getTimeSpendedOnTaskByUser().size() > 0) {
                Double sum = 0.0;
                for (TimeSpendedOnTaskByUser e : dto.getTimeSpendedOnTaskByUser()) {
                    if (e.getTimeEstimatedByUser() != null) {
                        sum += e.getTimeEstimatedByUser();
                    }
                }
                dto.setTotalSpendedTime(sum);
            }
            if (dto.getTotalSpendedTime() != null && dto.getAverageEstimation() != null) {
                if (dto.getTotalSpendedTime() > dto.getAverageEstimation()) {
                    tasksOvertimed.add(dto);
                }
            }
        }

        return new ResponseEntity<>(tasksOvertimed, HttpStatus.OK);
    }

    @GetMapping("/tasksNotOvertimed")
    @Timed
    public ResponseEntity<List<TaskDTO>> getTasksNotOvertimed() {
        log.debug("REST request to getTasksOvertimed");

        List<Task> tasks = taskService.findAllTasksOfCurrentLoggedUserCompany();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(tasks);
        List<TaskDTO> tasksOvertimed = new ArrayList<>();
        Instant now = Instant.now();
        for (TaskDTO dto : taskDTOS) {
            if (dto.getTotalSpendedTime() != null && dto.getAverageEstimation() != null) {
                if (dto.getTotalSpendedTime() < dto.getAverageEstimation()) {
                    tasksOvertimed.add(dto);
                }
            }
        }

        return new ResponseEntity<>(tasksOvertimed, HttpStatus.OK);
    }

    @GetMapping("/tasksCreatedToday")
    @Timed
    public ResponseEntity<List<TaskDTO>> getTasksCreatedToday() {
        log.debug("REST request to getTasksCreatedToday");

        List<Task> tasks = taskService.findAllTasksOfCurrentLoggedUserCompany();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(tasks);
        List<TaskDTO> tasksCreatedToday = new ArrayList<>();
        Instant now = Instant.now();
        for (Task t : tasks) {
            if (t.getCreatedDate().truncatedTo(ChronoUnit.DAYS).equals(now.truncatedTo(ChronoUnit.DAYS))) {
                tasksCreatedToday.add(taskMapper.convertToDto(t));
            }

        }

        return new ResponseEntity<>(tasksCreatedToday, HttpStatus.OK);
    }

    @GetMapping("/myTopAssigne")
    @Timed
    public ResponseEntity<List<UserAssigne>> getMyTopAssigne() {
        log.debug("REST request to getMyTopAssigne");

        List<Task> tasks = taskService.findAllTasksOfCurrentLoggedUserCompany();
        List<User> activeUsersFromLoggedUserCompany = userService.getActiveUsersFromLoggedUserCompany();
        List<UserAssigne> userAssignes = new ArrayList<>();
        Long id = userService.getLoggedUser().getId();
        for (User user : activeUsersFromLoggedUserCompany) {
            int countOfAssignedTasks = 0;
            SimpleUserDTO simpleUserDTO = simpleUserMapper.convertToDto(user);
            UserAssigne userAssigne = new UserAssigne();
            userAssigne.setUserDTO(simpleUserDTO);
            for (Task task : tasks) {
                if (task.getCreator().getId().equals(id) && task.getExecutor().getId().equals(user.getId())) {
                    countOfAssignedTasks++;
                }
            }
            userAssigne.setCount(countOfAssignedTasks);
            userAssignes.add(userAssigne);
        }
        Comparator<UserAssigne> byCount = Comparator.comparingInt(UserAssigne::getCount);
        List<UserAssigne> collect = userAssignes.stream().sorted(byCount.reversed()).collect(Collectors.toList());
        return new ResponseEntity<>(collect, HttpStatus.OK);
    }


    ////////////////////
    /////


    ////////////////////


    //
    @RequestMapping(value = "/findAllByCurrentUserExecutorIdAndTaskStatusCreated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TaskDTO>> getAllByCurrentUserExecutorIdAndTaskStatusCreated() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCurrentUserExecutorIdAndTaskStatusCreated : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCurrentUserExecutorIdAndTaskStatusCreated();
        List<TaskDTO> taskDTOS = taskMapper.convertToDto(allTasksOfCurrentLoggedUser);
        return new ResponseEntity<>(taskDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCurrentUserExecutorIdAndTaskStatusEnd", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCurrentUserExecutorIdAndTaskStatusEnd() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCurrentUserExecutorIdAndTaskStatusEnd : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCurrentUserExecutorIdAndTaskStatusEnd();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCurrentUserExecutorIdAndTaskStatusInprogress", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCurrentUserExecutorIdAndTaskStatusInprogress() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCurrentUserExecutorIdAndTaskStatusInprogress : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCurrentUserExecutorIdAndTaskStatusInprogress();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCurrentUserExecutorIdAndTaskStatusReopened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCurrentUserExecutorIdAndTaskStatusReopened() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCurrentUserExecutorIdAndTaskStatusReopened : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCurrentUserExecutorIdAndTaskStatusReopened();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCurrentUserExecutorIdAndTaskStatusOpened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCurrentUserExecutorIdAndTaskStatusOpened() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCurrentUserExecutorIdAndTaskStatusOpened : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCurrentUserExecutorIdAndTaskStatusOpened();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    //
    @RequestMapping(value = "/findAllByCreatorIdAndTaskStatusCreated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCreatorIdAndTaskStatusCreated() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCreatorIdAndTaskStatusCreated : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCreatorIdAndTaskStatusCreated();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCreatorIdAndTaskStatusEnd", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> findAllByCreatorIdAndTaskStatusEnd() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCreatorIdAndTaskStatusEnd : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCreatorIdAndTaskStatusEnd();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCreatorIdAndTaskStatusInprogress", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCreatorIdAndTaskStatusInprogress() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCreatorIdAndTaskStatusInprogress : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCreatorIdAndTaskStatusInprogress();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCreatorIdAndTaskStatusReopened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCreatorIdAndTaskStatusReopened() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCreatorIdAndTaskStatusReopened : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCreatorIdAndTaskStatusReopened();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/findAllByCreatorIdAndTaskStatusOpened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Task>> getAllByCreatorIdAndTaskStatusOpened() {
        User loggedUser = userService.getLoggedUser();
        log.debug("REST request to findAllByCreatorIdAndTaskStatusOpened : {} ", loggedUser);

        List<Task> allTasksOfCurrentLoggedUser = taskService.findAllByCreatorIdAndTaskStatusOpened();
        return new ResponseEntity<>(allTasksOfCurrentLoggedUser, HttpStatus.OK);
    }

    @GetMapping("/usersWithSkills")
    @Timed
    public ResponseEntity<List<UserSkill>> getUsersWithSkills() {
        log.debug("REST request to getUsersWithSkills");

        List<Skill> skills = skillService.findAll();
        List<User> activeUsersFromLoggedUserCompany = userService.getActiveUsersFromLoggedUserCompany();
        List<UserSkill> userSkills = new ArrayList<>();
        for (User u : activeUsersFromLoggedUserCompany) {
            SimpleUserDTO simpleUserDTO = simpleUserMapper.convertToDto(u);
            UserSkill userSkill = new UserSkill();
            userSkill.setSimpleUserDTO(simpleUserDTO);
            List<Skill> skillList = new ArrayList<>();
            for (Skill skill : skills) {
                if (u.getId().equals(skill.getUser().getId())) {
                    skillList.add(skill);
                }
            }
            userSkill.setSkill(skillMapper.convertToDto(skillList));
            userSkills.add(userSkill);
        }

        return new ResponseEntity<>(userSkills, HttpStatus.OK);
    }
}
