package io.github.jhipster.sample.web.rest.comment;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.sample.domain.TaskEstimation;
import io.github.jhipster.sample.domain.TimeSpendedOnTaskByUser;
import io.github.jhipster.sample.domain.User;
import io.github.jhipster.sample.repository.Comment.*;
import io.github.jhipster.sample.service.UserService;
import io.github.jhipster.sample.service.comment.CommentService;
import io.github.jhipster.sample.service.comment.TaskEstimationService;
import io.github.jhipster.sample.service.comment.TaskService;
import io.github.jhipster.sample.service.dto.MessageDTO;
import io.github.jhipster.sample.web.rest.MessageCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by PK on 2017-03-04.
 */
@RestController
@RequestMapping("/api/time")
public class TimeSpendedOnTaskByUserResource {

    private static final Logger log = LoggerFactory.getLogger(TimeSpendedOnTaskByUserResource.class);

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskPriorityRepository taskPriorityRepository;
    @Autowired
    private TaskEstimationRepository taskEstimationRepository;

    @Autowired
    private TaskStatusRepository taskStatusRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private TaskEstimationService taskEstimationService;
    @Autowired
    private TimeSpendedOnTaskByUserRepository timeSpendedOnTaskByUserRepository;


    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Boolean> xsltGenerator() {
        List<TaskEstimation> all = taskEstimationRepository.findAll();
        log.debug("ok: "+all);
        return new ResponseEntity<>( Boolean.TRUE, HttpStatus.OK);
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MessageDTO> add(@RequestBody TimeSpendedOnTaskByUser timeSpendedOnTaskByUser) {
        log.debug("REST request to create task");

        User loggedUser = userService.getLoggedUser();
        timeSpendedOnTaskByUser.setUser(loggedUser);
        List<TimeSpendedOnTaskByUser> all = timeSpendedOnTaskByUserRepository.findAll();

        timeSpendedOnTaskByUserRepository.saveAndFlush(timeSpendedOnTaskByUser);

        return new ResponseEntity<>(
                new MessageDTO(MessageCode.SUCCESS, MessageDTO.Message.GLOBAL_NOTIFY_SAVED.toString()),
                HttpStatus.CREATED);
    }


}
