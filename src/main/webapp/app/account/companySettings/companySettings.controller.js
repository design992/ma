'use strict';

var noPhotoUrl = 'assets/images/noPhoto.png'

angular.module('jhipsterSampleApplicationApp')
    .controller('CompanySettingsController', function ($scope, $timeout, $log, $translate, $window, $rootScope, Notify, Principal, Auth, CompanyService, PeopleService, CompanyInfoUpdater, BillingInfoUpdater, UserUpdater, $upload) {


        Principal.identity().then(function(account) {
            $rootScope.formChanged = false;

            $scope.updateUserInfo(account);

            UserUpdater.addUpdateCallback('CompanySettingsController', function (account) {
                $log.log("CompanySettingsController user update");
                $scope.updateUserInfo(account);
            });

            $scope.isAdmin = Principal.isInRole("ROLE_ADMIN");

            if($scope.isAdmin) {
                CompanyService.getCompanyInfo().then(function(companyAccount) {
                    $scope.settingsCompanyAccount = companyAccount;
                });

                // CompanyService.getBillingInfo().then(function(billingInfo) {
                //     $scope.wellBillingInfo = billingInfo;
                // });
                //
                // CompanyService.getCountries().then(function(countries) {
                //     $scope.countries = countries;
                // });
                //
                // CompanyService.getCardTypes().then(function(cardTypes) {
                //     $scope.cardTypes = cardTypes;
                // });

                PeopleService.getUsers($scope.settingsAccount.id).then(function(users) {
                    $scope.users = users;

                    //add indetifier for user (used in combo box)
                    for(var i in $scope.users){
                        if($scope.users[i].firstName==undefined){
                            $scope.users[i].identifier = $scope.users[i].login;
                        }else{
                            $scope.users[i].identifier = $scope.users[i].firstName;
                        }
                    }
                });

                BillingInfoUpdater.addUpdateCallback('CompanySettingsController', function (billingInfo) {
                    $log.log('BillingInfoUpdater(CompanySettingsController) update');
                    angular.copy(billingInfo,$scope.wellBillingInfo);
                });

                CompanyInfoUpdater.addUpdateCallback('CompanySettingsController', function (companyAccount) {
                    $log.log("CompanySettingsController CompanyInfo update");
                    $scope.settingsCompanyAccount = companyAccount;
                });

            }
        });

        $scope.getChosenCardType = function () {
            var cardType = "";
            angular.forEach($scope.cardTypes, function(value,key) {
                if($scope.wellBillingInfo){
                     if(value.id==$scope.wellBillingInfo.billingInfoCardType){
                        cardType = value.name;
                     }
                }
            });
            return cardType;
        };

        $scope.getChosenBillingCountryCode = function() {
            var code = "";
            angular.forEach($scope.countries, function(value,key) {
                if($scope.wellBillingInfo){
                     if(value.id==$scope.wellBillingInfo.billingInfoCountry){
                         code = value.code;
                     }
                }
            });
            return code;
        };

        $scope.saveCompany = function () {
            CompanyService.postCompanyInfoForm($scope.settingsCompanyAccount).then(function() {
                $rootScope.formChanged = false;
                CompanyInfoUpdater.update();
                Notify.addNotify('success',$translate.instant('global.notify.saved'));
            }).catch(function() {
                Notify.addNotify('error',$translate.instant('global.notify.notsaved'));
            });
        };

        $scope.updateUserInfo = function (account) {
            $scope.settingsAccount = account;
        }

        $scope.change = function() {
            $rootScope.formChanged = true;
        }

    });
