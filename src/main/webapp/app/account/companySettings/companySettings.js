'use strict';

angular.module('jhipsterSampleApplicationApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('companySettings', {
                parent: 'settings',
                url: '/companySettings',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'global.menu.account.settings'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/companySettings/companySettings.html',
                        controller: 'CompanySettingsController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('settings');
                        return $translate.refresh();
                    }]
                }
            });
    });
