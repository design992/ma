'use strict';

angular.module('jhipsterSampleApplicationApp')
	.service('BillingInfoUpdater', ['CompanyService', function (CompanyService) {

    var onUpdateCallbacks = {};
    var fireUpdateCallbacks = function () {
    	CompanyService.getBillingInfo().then(function(billingInfo) {
            for (var i in onUpdateCallbacks) {
                onUpdateCallbacks[i](billingInfo);
            }
        });
    };
    this.fireUpdateCallbacks = fireUpdateCallbacks;
    /**
     * Dodaje funkcję do funkcji wywoływanych podczas aktualizacji
     */
    this.addUpdateCallback = function (name, callback) {
        onUpdateCallbacks[name]=callback;
    };

    this.removeUpdateCallback = function (name) {
        delete onUpdateCallbacks[name];
    };

    this.update = function () {
    	fireUpdateCallbacks();
    };

}]);
