'use strict';

angular.module('jhipsterSampleApplicationApp')
    .factory('CompanyService', function ($rootScope, $http, Base64) {
        return {
        	postCompanyInfoForm: function (companyInfo) {
        	    return $http.post('api/companyInfo',companyInfo, {
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Basic " + Base64.encode("jhipsterSampleApplicationApp" + ':' + "mySecretOAuthSecret")
                    }
                }).then(function (response) {
                    return response.data;
                });
            },

            getCompanyInfo: function () {
                return $http.get('api/companyInfo').then(function (response) {
                    return response.data;
                });
            },

            getBillingInfo: function () {
                return $http.get('api/billingInfo').then(function (response) {
                    return response.data;
                });
            },

            postBillingInfoForm: function (billingInfo) {
        	    return $http.post('api/billingInfo',billingInfo, {
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Basic " + Base64.encode("agileToolBoxapp" + ':' + "mySecretOAuthSecret")
                    }
                }).then(function (response) {
                    return response.data;
                });
            },

            getCountries: function () {
            	return $http.get('api/countries').then(function (response) {
                    return response.data;
                });
            },

            getUsers: function() {
            	return $http.get('api/users').then(function (response) {
                    return response.data;
                });
            },

            getCardTypes: function() {
            	return $http.get('api/cardTypes').then(function (response) {
                    return response.data;
                });
            }

        };
    });
