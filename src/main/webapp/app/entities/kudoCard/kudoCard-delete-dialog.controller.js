(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('KudoCardDeleteController',KudoCardDeleteController);

    KudoCardDeleteController.$inject = ['$uibModalInstance', 'entity', 'KudoCard'];

    function KudoCardDeleteController($uibModalInstance, entity, KudoCard) {
        var vm = this;

        vm.kudoCard = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            KudoCard.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
