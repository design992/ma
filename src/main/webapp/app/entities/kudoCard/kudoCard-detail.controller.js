(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('KudoCardDetailController', KudoCardDetailController);

    KudoCardDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'KudoCard', 'Operation'];

    function KudoCardDetailController($scope, $rootScope, $stateParams, previousState, entity, KudoCard, Operation) {
        var vm = this;

        vm.kudoCard = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jhipsterSampleApplicationApp:labelUpdate', function(event, result) {
            vm.kudoCard = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
