(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('KudoCardDialogController', KudoCardDialogController);

    KudoCardDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'KudoCard', 'Operation'];

    function KudoCardDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, KudoCard, Operation) {
        var vm = this;

        vm.kudoCard = entity;
        vm.clear = clear;
        vm.save = save;
        vm.operations = Operation.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.kudoCard.id !== null) {
                KudoCard.update(vm.kudoCard, onSaveSuccess, onSaveError);
            } else {
                KudoCard.save(vm.kudoCard, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jhipsterSampleApplicationApp:kudoCardUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
