(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('KudoCardController', KudoCardController);

    KudoCardController.$inject = ['KudoCard'];

    function KudoCardController(KudoCard) {

        var vm = this;

        vm.kudoCard = [];

        loadAll();

        function loadAll() {
            KudoCard.query(function(result) {
                vm.kudoCard = result;
                vm.searchQuery = null;
            });
        }
    }
})();
