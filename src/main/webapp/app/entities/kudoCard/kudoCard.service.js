(function() {
    'use strict';
    angular
        .module('jhipsterSampleApplicationApp')
        .factory('KudoCard', KudoCard);

    KudoCard.$inject = ['$resource'];

    function KudoCard ($resource) {
        var resourceUrl =  'api/kudoCard/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
