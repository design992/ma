(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('kudoCard', {
            parent: 'entity',
            url: '/kudoCard',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jhipsterSampleApplicationApp.label.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/kudoCard/kudoCards.html',
                    controller: 'KudoCardController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('kudoCard');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('kudoCard-detail', {
            parent: 'kudoCard',
            url: '/kudoCard/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jhipsterSampleApplicationApp.label.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/kudoCard/kudoCard-detail.html',
                    controller: 'KudoCardDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('kudoCard');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'KudoCard', function($stateParams, KudoCard) {
                    return KudoCard.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'kudoCard',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('kudoCard-detail.edit', {
            parent: 'kudoCard-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/kudoCard/kudoCard-dialog.html',
                    controller: 'KudoCardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['KudoCard', function(KudoCard) {
                            return KudoCard.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('kudoCard.new', {
            parent: 'kudoCard',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/kudoCard/kudoCard-dialog.html',
                    controller: 'KudoCardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('kudoCard', null, { reload: 'kudoCard' });
                }, function() {
                    $state.go('kudoCard');
                });
            }]
        })
        .state('kudoCard.edit', {
            parent: 'kudoCard',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/kudoCard/kudoCard-dialog.html',
                    controller: 'KudoCardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['KudoCard', function(KudoCard) {
                            return KudoCard.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('kudoCard', null, { reload: 'kudoCard' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('kudoCard.delete', {
            parent: 'kudoCard',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/kudoCard/kudoCard-delete-dialog.html',
                    controller: 'KudoCardDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['KudoCard', function(KudoCard) {
                            return KudoCard.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('kudoCard', null, { reload: 'kudoCard' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
