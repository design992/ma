(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('SkillController', SkillController);

    SkillController.$inject = ['Skill'];

    function SkillController(Skill) {

        var vm = this;

        vm.skills = [];

        loadAll();

        function loadAll() {
            Skill.query(function(result) {
                vm.skills = result;
                vm.searchQuery = null;
            });
        }
    }
})();
