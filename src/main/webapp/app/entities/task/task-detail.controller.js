(function () {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('TaskDetailController', TaskDetailController);

    TaskDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Task', 'User', 'Operation', 'TaskPriorities', 'TaskStatuses', 'TaskEstimation', 'TaskEstimationSample', 'PostAddEstimationForm'];

    function TaskDetailController($scope, $rootScope, $stateParams, previousState, entity, Task, User, Operation, TaskPriorities, TaskStatuses, TaskEstimation, TaskEstimationSample, PostAddEstimationForm) {
        var vm = this;

        vm.task = entity;
        //vm.task=Task.query();
        // vm.task=loadAll();
        vm.previousState = previousState.name;
        vm.taskPriorities = [];
        vm.taskStatuses = [];
        vm.taskEstimation = {};


        function loadAll() {
            Task.query(function (result) {
                vm.tasks = result;
                vm.searchQuery = null;
            });
        }
        //
        // function sendEstimation() {
        //     TaskPriorities.query(function (result) {
        //         vm.taskPriorities = result;
        //         vm.searchQuery = null;
        //     });
        // }
        vm.sendEstimation = function () {
            alert('sendestimation');
            PostAddEstimationForm.save(function (result) {
                vm.taskEstimation = result;
                vm.searchQuery = null;
            });
            // PostAddEstimationForm(vm.taskEstimation).then(function (data) {
            //     $log.log("data estimation :"+data);
            //
            //     vm.taskEstimation = {};
            //     $scope.close();
            // }).catch(function () {
            //     alert('error post estimation')
            // });
        };

        // vm.taskEstimation.task.id=vm.task.id;
        loadTaskEstimationSample();
        //
        // function save () {
        //     vm.isSaving = true;
        //     if (vm.taskEstimation.id !== null) {
        //         TaskEstimation.update(vm.taskEstimation, onSaveSuccess, onSaveError);
        //     } else {
        //         TaskEstimation.save(vm.taskEstimation, onSaveSuccess, onSaveError);
        //     }
        // }

        function onSaveSuccess(result) {
            $scope.$emit('jhipsterSampleApplicationApp:taskEstimationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function loadTaskEstimationSample() {
            TaskEstimationSample.query(function (result) {
                vm.taskEstimation = result;
                vm.searchQuery = null;
            });
        }

        loadTaskPriorities();
        loadTaskStatuses();
        function loadTaskPriorities() {
            TaskPriorities.query(function (result) {
                vm.taskPriorities = result;
                vm.searchQuery = null;
            });
        }

        function loadTaskStatuses() {
            TaskStatuses.query(function (result) {
                vm.taskStatuses = result;
                vm.searchQuery = null;
            });
        }

        var unsubscribe = $rootScope.$on('jhipsterSampleApplicationApp:taskUpdate', function (event, result) {
            vm.task = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
