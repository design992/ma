(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('TaskDialogController', TaskDialogController);

    TaskDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Task', 'User', 'Operation','TaskPriorities','TaskStatuses'];

    function TaskDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Task, User, Operation,TaskPriorities,TaskStatuses) {
        var vm = this;

        vm.task = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.operations = Operation.query();
        vm.taskPriorities=[];
        vm.taskStatuses=[];

        loadTaskPriorities();
        loadTaskStatuses();
        function loadTaskPriorities() {
            TaskPriorities.query(function (result) {
                vm.taskPriorities = result;
                vm.searchQuery = null;
            });
        }
        function loadTaskStatuses() {
            TaskStatuses.query(function (result) {
                vm.taskStatuses = result;
                vm.searchQuery = null;
            });
        }


        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.task.id !== null) {
                Task.update(vm.task, onSaveSuccess, onSaveError);
            } else {
                Task.save(vm.task, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jhipsterSampleApplicationApp:taskUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
