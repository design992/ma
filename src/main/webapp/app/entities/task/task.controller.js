(function () {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('TaskController', TaskController);

    TaskController.$inject = ['Task', 'TasksOfCurrentLoggedUser', 'CreatorAndPriorityLow', 'CreatorHigh', 'CreatorNormal','WriteXmlFile','CountOfTasks','CountOfCreators','TaskPriorityWithCount','TaskStatusWithCount','TaskPriorities','TaskStatuses','PostAddEstimationForm','TasksEndToday','TasksStartToday','ExecutorAndPriorityLow', 'ExecutorHigh', 'ExecutorNormal','MyTopAssigne','tasksToEstimate','tasksEstimated','tasksOvertimed','findAllByCurrentUserExecutorIdAndTaskStatusCreated','findAllByCurrentUserExecutorIdAndTaskStatusEnd','findAllByCurrentUserExecutorIdAndTaskStatusInprogress','findAllByCurrentUserExecutorIdAndTaskStatusReopened','findAllByCurrentUserExecutorIdAndTaskStatusOpened','findAllByCreatorIdAndTaskStatusCreated','findAllByCreatorIdAndTaskStatusEnd','findAllByCreatorIdAndTaskStatusInprogress','findAllByCreatorIdAndTaskStatusReopened','findAllByCreatorIdAndTaskStatusOpened','$window','usersWithSkills'];

    function TaskController(Task, TasksOfCurrentLoggedUser, CreatorAndPriorityLow, CreatorHigh, CreatorNormal,WriteXmlFile,CountOfTasks,CountOfCreators,TaskPriorityWithCount,TaskStatusWithCount,TaskPriorities,TaskStatuses,PostAddEstimationForm,TasksEndToday,TasksStartToday,ExecutorAndPriorityLow, ExecutorHigh, ExecutorNormal,MyTopAssigne,tasksToEstimate,tasksEstimated,tasksOvertimed,findAllByCurrentUserExecutorIdAndTaskStatusCreated,findAllByCurrentUserExecutorIdAndTaskStatusEnd,findAllByCurrentUserExecutorIdAndTaskStatusInprogress,findAllByCurrentUserExecutorIdAndTaskStatusReopened,findAllByCurrentUserExecutorIdAndTaskStatusOpened,findAllByCreatorIdAndTaskStatusCreated,findAllByCreatorIdAndTaskStatusEnd,findAllByCreatorIdAndTaskStatusInprogress,findAllByCreatorIdAndTaskStatusReopened,findAllByCreatorIdAndTaskStatusOpened,$window,usersWithSkills) {

        var vm = this;

        vm.tasks = [];
        vm.MyTopAssigne=[];
        vm.tasksToEstimate=[];
        vm.tasksEstimated=[];
        vm.tasksOvertimed=[];
        vm.usersWithSkills=[];

        //
        // vm.MyTopAssigne=vm.MyTopAssigne.sort(function (x, y) {
        //     return x.count > y.count;
        // })
        vm.tasksOfCurrentLoggedUser = [];
        vm.findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh = [];
        vm.findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow = [];
        vm.findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal = [];



        vm.findAllByCurrentUserExecutorIdAndTaskStatusCreated = [];
        vm.findAllByCurrentUserExecutorIdAndTaskStatusEnd= [];
        vm.findAllByCurrentUserExecutorIdAndTaskStatusInprogress= [];
        vm.findAllByCurrentUserExecutorIdAndTaskStatusReopened= [];
        vm.findAllByCurrentUserExecutorIdAndTaskStatusOpened= [];

        vm.findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh = [];
        vm.findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow = [];
        vm.findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal = [];

        vm.write=[];
        vm.tasksCount=[];
        vm.countOfCreators=[];
        vm.priorityWithCountList=[];
        vm.statusWithCountList=[];
        vm.taskPriorities=[];
        vm.tasksEndToday=[];
        vm.tasksStartToday=[];

        vm.findAllByCurrentUserCreatorIdAndTaskStatusCreated = [];
        vm.findAllByCurrentUserCreatorIdAndTaskStatusEnd= [];
        vm.findAllByCurrentUserCreatorIdAndTaskStatusInprogress= [];
        vm.findAllByCurrentUserCreatorIdAndTaskStatusReopened= [];
        vm.findAllByCurrentUserCreatorIdAndTaskStatusOpened= [];

        loadfindAllByCurrentUserCreatorIdAndTaskStatusCreated();
        loadfindAllByCurrentUserCreatorIdAndTaskStatusEnd();
        loadfindAllByCurrentUserCreatorIdAndTaskStatusInprogress();
        loadfindAllByCurrentUserCreatorIdAndTaskStatusReopened();
        loadfindAllByCurrentUserCreatorIdAndTaskStatusOpened();

        loadAll();
        loadUsersWithSkills();
        loadMyTopAssigne();
        loadTasksToEstimate();
        loadTasksEstimated();
        loadTasksOvertimed();
        loadAlltasksOfCurrentLoggedUser();
        loadAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh();
        loadAllTasksOfCurrentLoggedUserCreatorAndPriorityLow();
        loadAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal();
        loadAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh();
        loadAllTasksOfCurrentLoggedUserExecutorAndPriorityLow();
        loadAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal();
        writeToFile();
        loadCountOfTasks();
        loadCountOfCreators();
        loadTaskPriorityWithCount();
        loadTaskStatusWithCount();
        loadTaskPriorities();
        loadTaskStatuses();
        loadTasksEndToday();
        loadTasksStartToday();

        loadfindAllByCurrentUserExecutorIdAndTaskStatusCreated();
        loadfindAllByCurrentUserExecutorIdAndTaskStatusEnd();
        loadfindAllByCurrentUserExecutorIdAndTaskStatusInprogress();
        loadfindAllByCurrentUserExecutorIdAndTaskStatusReopened();
        loadfindAllByCurrentUserExecutorIdAndTaskStatusOpened();

        vm.export = function(){
            var table = document.getElementById('exportthis').innerHTML;
            var myWindow = $window.open('', '', 'width=800, height=600');
            myWindow.document.write(table);
            myWindow.print();
        };

        vm.dateFilter =function () {
            return function (input, start, end) {
                var inputDate = new Date(input),
                    startDate = new Date(start),
                    endDate = new Date(end),
                    result = [];

                for (var i = 0, len = input.length; i < len; i++) {
                    inputDate = new Date(input[i].beginDate);
                    if (startDate < inputDate && inputDate < endDate) {
                        result.push(input[i]);
                    }
                }
            }
        };

///////////
//         vm.export = function(){
//             html2canvas(document.getElementById('exportthis'), {
//                 onrendered: function (canvas) {
//                     var data = canvas.toDataURL();
//                     var docDefinition = {
//                         content: [{
//                             image: data,
//                             width: 500,
//                         }]
//                     };
//                     pdfMake.createPdf(docDefinition).download("test.pdf");
//                 }
//             });
//         }
        function loadfindAllByCurrentUserCreatorIdAndTaskStatusCreated() {
            findAllByCreatorIdAndTaskStatusCreated.query(function (result) {
                vm.findAllByCurrentUserCreatorIdAndTaskStatusCreated = result;
                vm.searchQuery = null;
            });
        }
        function loadUsersWithSkills() {
            usersWithSkills.query(function (result) {
                vm.usersWithSkills = result;
                vm.searchQuery = null;
            });
        }

        function loadfindAllByCurrentUserCreatorIdAndTaskStatusEnd() {
            findAllByCreatorIdAndTaskStatusEnd.query(function (result) {
                vm.findAllByCurrentUserCreatorIdAndTaskStatusEnd = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserCreatorIdAndTaskStatusInprogress() {
            findAllByCreatorIdAndTaskStatusInprogress.query(function (result) {
                vm.findAllByCurrentUserCreatorIdAndTaskStatusInprogress = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserCreatorIdAndTaskStatusReopened() {
            findAllByCreatorIdAndTaskStatusReopened.query(function (result) {
                vm.findAllByCurrentUserCreatorIdAndTaskStatusReopened = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserCreatorIdAndTaskStatusOpened() {
            findAllByCreatorIdAndTaskStatusOpened.query(function (result) {
                vm.findAllByCurrentUserCreatorIdAndTaskStatusOpened = result;
                vm.searchQuery = null;
            });
        }

        ////////////////////////




///////////

        function loadfindAllByCurrentUserExecutorIdAndTaskStatusCreated() {
            findAllByCurrentUserExecutorIdAndTaskStatusCreated.query(function (result) {
                vm.findAllByCurrentUserExecutorIdAndTaskStatusCreated = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserExecutorIdAndTaskStatusEnd() {
            findAllByCurrentUserExecutorIdAndTaskStatusEnd.query(function (result) {
                vm.findAllByCurrentUserExecutorIdAndTaskStatusEnd = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserExecutorIdAndTaskStatusInprogress() {
            findAllByCurrentUserExecutorIdAndTaskStatusInprogress.query(function (result) {
                vm.findAllByCurrentUserExecutorIdAndTaskStatusInprogress = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserExecutorIdAndTaskStatusReopened() {
            findAllByCurrentUserExecutorIdAndTaskStatusReopened.query(function (result) {
                vm.findAllByCurrentUserExecutorIdAndTaskStatusReopened = result;
                vm.searchQuery = null;
            });
        }
        function loadfindAllByCurrentUserExecutorIdAndTaskStatusOpened() {
            findAllByCurrentUserExecutorIdAndTaskStatusOpened.query(function (result) {
                vm.findAllByCurrentUserExecutorIdAndTaskStatusOpened = result;
                vm.searchQuery = null;
            });
        }

        ////////////////////////

        function loadTasksStartToday() {
            TasksStartToday.query(function (result) {
                vm.tasksStartToday = result;
                vm.searchQuery = null;
            });
        }
        function loadTasksOvertimed() {
            tasksOvertimed.query(function (result) {
                vm.tasksOvertimed = result;
                vm.searchQuery = null;
            });
        }

        function loadMyTopAssigne() {
            MyTopAssigne.query(function (result) {
                vm.MyTopAssigne = result;
                vm.searchQuery = null;
            });
        }
        function loadTasksToEstimate() {
            tasksToEstimate.query(function (result) {
                vm.tasksToEstimate = result;
                vm.searchQuery = null;
            });
        }
        function loadTasksEstimated() {
            tasksEstimated.query(function (result) {
                vm.tasksEstimated = result;
                vm.searchQuery = null;
            });
        }


        function loadTasksEndToday() {
            TasksEndToday.query(function (result) {
                vm.tasksEndToday = result;
                vm.searchQuery = null;
            });
        }
        function loadCountOfCreators() {
            CountOfCreators.query(function (result) {
                vm.countOfCreators = result;
                vm.searchQuery = null;
            });
        }

        function loadTaskPriorities() {
            TaskPriorities.query(function (result) {
                vm.taskPriorities = result;
                vm.searchQuery = null;
            });
        }
        function loadTaskStatuses() {
            TaskStatuses.query(function (result) {
                vm.taskStatuses = result;
                vm.searchQuery = null;
            });
        }


        function loadTaskPriorityWithCount() {
            TaskPriorityWithCount.query(function (result) {
                vm.priorityWithCountList = result;
                vm.searchQuery = null;
            });
        }
        function loadTaskStatusWithCount() {
            TaskStatusWithCount.query(function (result) {
                vm.statusWithCountList = result;
                vm.searchQuery = null;
            });
        }

        function loadCountOfTasks() {
            CountOfTasks.query(function (result) {
                vm.tasksCount = result;
                vm.searchQuery = null;
            });
        }

        function writeToFile() {
            WriteXmlFile.query(function (result) {
                vm.write = result;
                vm.searchQuery = null;
            });
        }
        function loadAll() {
            Task.query(function (result) {
                vm.tasks = result;
                vm.searchQuery = null;
            });
        }

        function loadAlltasksOfCurrentLoggedUser() {
            TasksOfCurrentLoggedUser.query(function (result) {
                vm.tasksOfCurrentLoggedUser = result;
                vm.searchQuery = null;
            });
        }

        function loadAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh() {
            CreatorHigh.query(function (result) {
                vm.findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh = result;
                vm.searchQuery = null;
            });
        }

        function loadAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal() {
            CreatorNormal.query(function (result) {
                vm.findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal = result;
                vm.searchQuery = null;
            });
        }

        function loadAllTasksOfCurrentLoggedUserCreatorAndPriorityLow() {
            CreatorAndPriorityLow.query(function (result) {
                vm.findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow = result;
                vm.searchQuery = null;
            });
        }


        function loadAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh() {
            ExecutorHigh.query(function (result) {
                vm.findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh = result;
                vm.searchQuery = null;
            });
        }

        function loadAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal() {
            ExecutorNormal.query(function (result) {
                vm.findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal = result;
                vm.searchQuery = null;
            });
        }

        function loadAllTasksOfCurrentLoggedUserExecutorAndPriorityLow() {
            ExecutorAndPriorityLow.query(function (result) {
                vm.findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow = result;
                vm.searchQuery = null;
            });
        }
        vm.saveAsXlsx = function (list) {
            alasql('SELECT b.simpleUserDTO.login as LOGIN,b.simpleUserDTO.firstName as FIRST_NAME,b.kount as COUNT_OF_TASKS INTO XLSX("output.xlsx",{headers:true}) FROM ? b', [list]);
        }
        vm.saveAsCSV = function (list) {
            alasql('SELECT b.simpleUserDTO.login as LOGIN,b.simpleUserDTO.firstName as FIRST_NAME,b.kount as COUNT_OF_TASKS INTO XLSX("outputCSV.csv",{headers:true}) FROM ? b', [list]);
        }

        vm.saveAsXlsxTask = function (list) {
            alasql('SELECT b.id as ID, b.name as NAME, b.description as DESCRIPTION,b.creator.firstName as CREATOR, b.executor.firstName AS EXECUTOR,b.beginDate as BEGIN_DATE,b.endDate as END_DATE, b.spendedTime as SPENDED_TIME INTO XLSX("output.xlsx",{headers:true}) FROM ? b', [list]);
        }
        vm.saveAsCSVTask = function (list) {
            alasql('SELECT b.id as ID, b.name as NAME, b.description as DESCRIPTION,b.creator.firstName as CREATOR, b.executor.firstName AS EXECUTOR,b.beginDate as BEGIN_DATE,b.endDate as END_DATE, b.spendedTime as SPENDED_TIME INTO XLSX("outputCSV.csv",{headers:true}) FROM ? b', [list]);
        }

        // vm.saveAsPDF = function (list) {
        //     var json = doc.autoTableHtmlToJson(document.getElementById("exportthis"));
        //     var cols = [], data = [];
        //     for(var i = 0; i < json.length; i++)  {
        //         var row = json[i];
        //         var newRow = [];
        //         for (var key in row) {
        //             if (row.hasOwnProperty(key)) {
        //                 if(i === 0) {
        //                     cols.push(key);
        //                 }
        //                 newRow.push(row[key]);
        //             }
        //         }
        //         data.push(newRow);
        //     }
        //     doc.autoTable(cols, data, {startY: 60});
        //     doc.save("table.pdf");
        // }

        vm.saveAsPDF = function (list) {
            var columns = [
                {title: "ID", dataKey: "id"},
                {title: "Name", dataKey: "name"},
                {title: "Description", dataKey: "description"},
                {title: "Begin date", dataKey: "beginDate"},
                {title: "End date", dataKey: "endDate"},
            ];

            var rows = list;
            // Only pt supported (not mm or in)
            var doc = new jsPDF('p', 'pt');
            doc.autoTable(columns, rows, {
                styles: {fillColor: [100, 255, 255]},
                columnStyles: {
                    id: {fillColor: 255}
                },
                margin: {top: 60},
                addPageContent: function (data) {
                    doc.text("Tasks", 40, 30);
                }
            });
            doc.save('table.pdf');
        }
    }
})();
