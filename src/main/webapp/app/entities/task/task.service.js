(function() {
    'use strict';
    angular
        .module('jhipsterSampleApplicationApp')
        .factory('Task', Task)
        .factory('TasksOfCurrentLoggedUser', TasksOfCurrentLoggedUser)
        .factory('CreatorAndPriorityLow',CreatorAndPriorityLow)
        .factory('CreatorHigh',CreatorHigh)
        .factory('CreatorNormal',CreatorNormal)
        .factory('ExecutorAndPriorityLow',ExecutorAndPriorityLow)
        .factory('ExecutorHigh',ExecutorHigh)
        .factory('ExecutorNormal',ExecutorNormal)
        .factory('WriteXmlFile',WriteXmlFile)
        .factory('CountOfTasks',CountOfTasks)
        .factory('CountOfCreators',CountOfCreators)
        .factory('TaskPriorityWithCount',TaskPriorityWithCount)
        .factory('TaskStatusWithCount',TaskStatusWithCount)
        .factory('TaskPriorities',TaskPriorities)
        .factory('TaskEstimation',TaskEstimation)
        .factory('TaskEstimationSample',TaskEstimationSample)
        .factory('PostAddEstimationForm',PostAddEstimationForm)
        .factory('TasksEndToday',TasksEndToday)
        .factory('TasksStartToday',TasksStartToday)
        .factory('MyTopAssigne',MyTopAssigne)
        .factory('tasksToEstimate',tasksToEstimate)
        .factory('tasksEstimated',tasksEstimated)
        .factory('tasksOvertimed',tasksOvertimed)
        .factory('usersWithSkills',usersWithSkills)
        .factory('findAllByCurrentUserExecutorIdAndTaskStatusCreated',findAllByCurrentUserExecutorIdAndTaskStatusCreated)
        .factory('findAllByCurrentUserExecutorIdAndTaskStatusEnd',findAllByCurrentUserExecutorIdAndTaskStatusEnd)
        .factory('findAllByCurrentUserExecutorIdAndTaskStatusInprogress',findAllByCurrentUserExecutorIdAndTaskStatusInprogress)
        .factory('findAllByCurrentUserExecutorIdAndTaskStatusReopened',findAllByCurrentUserExecutorIdAndTaskStatusReopened)
        .factory('findAllByCurrentUserExecutorIdAndTaskStatusOpened',findAllByCurrentUserExecutorIdAndTaskStatusOpened)
        .factory('findAllByCreatorIdAndTaskStatusCreated',findAllByCreatorIdAndTaskStatusCreated)
        .factory('findAllByCreatorIdAndTaskStatusEnd',findAllByCreatorIdAndTaskStatusEnd)
        .factory('findAllByCreatorIdAndTaskStatusInprogress',findAllByCreatorIdAndTaskStatusInprogress)
        .factory('findAllByCreatorIdAndTaskStatusReopened',findAllByCreatorIdAndTaskStatusReopened)
        .factory('findAllByCreatorIdAndTaskStatusOpened',findAllByCreatorIdAndTaskStatusOpened)
        .factory('TaskStatuses',TaskStatuses);


    ////////////

    findAllByCreatorIdAndTaskStatusCreated.$inject = ['$resource'];

    function findAllByCreatorIdAndTaskStatusCreated ($resource) {
        var resourceUrl =  'api/findAllByCreatorIdAndTaskStatusCreated';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    usersWithSkills.$inject = ['$resource'];

    function usersWithSkills ($resource) {
        var resourceUrl =  'api/usersWithSkills';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    findAllByCreatorIdAndTaskStatusEnd.$inject = ['$resource'];

    function findAllByCreatorIdAndTaskStatusEnd ($resource) {
        var resourceUrl =  'api/findAllByCreatorIdAndTaskStatusEnd';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCreatorIdAndTaskStatusInprogress.$inject = ['$resource'];

    function findAllByCreatorIdAndTaskStatusInprogress ($resource) {
        var resourceUrl =  'api/findAllByCreatorIdAndTaskStatusInprogress';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCreatorIdAndTaskStatusReopened.$inject = ['$resource'];

    function findAllByCreatorIdAndTaskStatusReopened ($resource) {
        var resourceUrl =  'api/findAllByCreatorIdAndTaskStatusReopened';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCreatorIdAndTaskStatusOpened.$inject = ['$resource'];

    function findAllByCreatorIdAndTaskStatusOpened ($resource) {
        var resourceUrl =  'api/findAllByCreatorIdAndTaskStatusOpened';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    //////////


    ////////////

    findAllByCurrentUserExecutorIdAndTaskStatusCreated.$inject = ['$resource'];

    function findAllByCurrentUserExecutorIdAndTaskStatusCreated ($resource) {
        var resourceUrl =  'api/findAllByCurrentUserExecutorIdAndTaskStatusCreated';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCurrentUserExecutorIdAndTaskStatusEnd.$inject = ['$resource'];

    function findAllByCurrentUserExecutorIdAndTaskStatusEnd ($resource) {
        var resourceUrl =  'api/findAllByCurrentUserExecutorIdAndTaskStatusEnd';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCurrentUserExecutorIdAndTaskStatusInprogress.$inject = ['$resource'];

    function findAllByCurrentUserExecutorIdAndTaskStatusInprogress ($resource) {
        var resourceUrl =  'api/findAllByCurrentUserExecutorIdAndTaskStatusInprogress';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCurrentUserExecutorIdAndTaskStatusReopened.$inject = ['$resource'];

    function findAllByCurrentUserExecutorIdAndTaskStatusReopened ($resource) {
        var resourceUrl =  'api/findAllByCurrentUserExecutorIdAndTaskStatusReopened';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    findAllByCurrentUserExecutorIdAndTaskStatusOpened.$inject = ['$resource'];

    function findAllByCurrentUserExecutorIdAndTaskStatusOpened ($resource) {
        var resourceUrl =  'api/findAllByCurrentUserExecutorIdAndTaskStatusOpened';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    //////////

    MyTopAssigne.$inject = ['$resource'];

    function MyTopAssigne ($resource) {
        var resourceUrl =  'api/myTopAssigne';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    tasksToEstimate.$inject = ['$resource'];

    function tasksToEstimate ($resource) {
        var resourceUrl =  'api/tasksToEstimate';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    tasksOvertimed.$inject = ['$resource'];

    function tasksOvertimed ($resource) {
        var resourceUrl =  'api/tasksOvertimed';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };


    tasksEstimated.$inject = ['$resource'];

    function tasksEstimated ($resource) {
        var resourceUrl =  'api/tasksEstimated';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    TaskStatuses.$inject = ['$resource'];

    function TaskStatuses ($resource) {
        var resourceUrl =  'api/taskStatus';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };


    TasksStartToday.$inject = ['$resource'];

    function TasksStartToday ($resource) {
        var resourceUrl =  'api/tasksBeginToday';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    TasksEndToday.$inject = ['$resource'];

    function TasksEndToday ($resource) {
        var resourceUrl =  'api/tasksEndsToday';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    PostAddEstimationForm.$inject = ['$resource'];

    function PostAddEstimationForm ($resource) {
        var resourceUrl =  'api/addTaskEstimation';

        return $resource(resourceUrl, {}, {
            postMethod:{
                url:'http://localhost:8080/#/api/addTaskEstimation',
                method:'POST',
                isArray:false
            }
        });
    };
    //
    // function PostAddEstimationForm (estimation) {
    //     return $http.patch('/api/addTaskEstimation',estimation, {
    //         headers: {
    //             "Content-Type": "application/json",
    //             "Accept": "application/json",
    //             "Authorization": "Basic " + Base64.encode("agileToolBoxapp" + ':' + "mySecretOAuthSecret")
    //         }
    //     }).then(function (response) {
    //         return response.data;
    //     });
    // }

    TaskEstimationSample.$inject = ['$resource'];

    function TaskEstimationSample ($resource) {
        var resourceUrl =  'api/estimationSample';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: false},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    TaskPriorities.$inject = ['$resource'];

    function TaskPriorities ($resource) {
        var resourceUrl =  'api/taskPriorities';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    WriteXmlFile.$inject = ['$resource'];

    function WriteXmlFile($resource) {
        var resourceUrl =  'api/xmlGenerator';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    TaskPriorityWithCount.$inject = ['$resource'];

    function TaskPriorityWithCount ($resource) {
        var resourceUrl =  'api/getTaskPrioritiesWithCount';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    TaskStatusWithCount.$inject = ['$resource'];
    function TaskStatusWithCount ($resource) {
        var resourceUrl =  'api/getTaskStatusesWithCount';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    TasksOfCurrentLoggedUser.$inject = ['$resource'];

    function TasksOfCurrentLoggedUser ($resource) {
        var resourceUrl =  'api/allTasksOfCurrentLoggedUser';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    CreatorHigh.$inject = ['$resource'];

    function CreatorHigh ($resource) {
        var resourceUrl =  'api/findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    CreatorNormal.$inject = ['$resource'];

    function CreatorNormal ($resource) {
        var resourceUrl =  'api/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    CreatorAndPriorityLow.$inject = ['$resource'];

    function CreatorAndPriorityLow ($resource) {
        var resourceUrl =  'api/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };

    //ecexutor

    ExecutorHigh.$inject = ['$resource'];

    function ExecutorHigh ($resource) {
        var resourceUrl =  'api/findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    ExecutorNormal.$inject = ['$resource'];

    function ExecutorNormal ($resource) {
        var resourceUrl =  'api/findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };
    ExecutorAndPriorityLow.$inject = ['$resource'];

    function ExecutorAndPriorityLow ($resource) {
        var resourceUrl =  'api/findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    };


    TaskEstimation.$inject = ['$resource'];

    function TaskEstimation ($resource) {
        var resourceUrl =  'api/addTaskEstimation';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }

    Task.$inject = ['$resource'];

    function Task ($resource) {
        var resourceUrl =  'api/tasks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
    CountOfCreators.$inject = ['$resource'];

    function CountOfCreators ($resource) {
        var resourceUrl =  'api/getCountOfTasksCreatedByUsersFromCurrentUserCompany';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
    CountOfTasks.$inject = ['$resource'];

    function CountOfTasks ($resource) {
        var resourceUrl =  'api/getCountOfTasksAssignedToUsersFromCurrentUserCompany';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }

})();
