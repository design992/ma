(function () {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh', {
                parent: 'entity',
                url: '/findAllTasksOfCurrentLoggedUserExecutorAndPriorityHigh',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUserExecutorWithPriorityHigh.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('usersWithSkills', {
                parent: 'entity',
                url: '/usersWithSkills',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Users With Skills'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/usersWithSkills.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        //////////
        $stateProvider
            .state('findAllByCreatorIdAndTaskStatusCreated', {
                parent: 'entity',
                url: '/findAllByCreatorIdAndTaskStatusCreated',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserCreatorIdAndTaskStatusCreated.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCreatorIdAndTaskStatusEnd', {
                parent: 'entity',
                url: '/findAllByCreatorIdAndTaskStatusEnd',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserCreatorIdAndTaskStatusEnd.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCreatorIdAndTaskStatusInprogress', {
                parent: 'entity',
                url: '/findAllByCreatorIdAndTaskStatusInprogress',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserCreatorIdAndTaskStatusInprogress.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCreatorIdAndTaskStatusReopened', {
                parent: 'entity',
                url: '/findAllByCreatorIdAndTaskStatusReopened',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserCreatorIdAndTaskStatusReopened.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCreatorIdAndTaskStatusOpened', {
                parent: 'entity',
                url: '/findAllByCreatorIdAndTaskStatusOpened',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserCreatorIdAndTaskStatusOpened.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })

        //////////////
        /////////////////
        $stateProvider
            .state('findAllByCurrentUserExecutorIdAndTaskStatusCreated', {
                parent: 'entity',
                url: '/findAllByCurrentUserExecutorIdAndTaskStatusCreated',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserExecutorIdAndTaskStatusCreated.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCurrentUserExecutorIdAndTaskStatusEnd', {
                parent: 'entity',
                url: '/findAllByCurrentUserExecutorIdAndTaskStatusEnd',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserExecutorIdAndTaskStatusEnd.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCurrentUserExecutorIdAndTaskStatusInprogress', {
                parent: 'entity',
                url: '/findAllByCurrentUserExecutorIdAndTaskStatusInprogress',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserExecutorIdAndTaskStatusInprogress.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCurrentUserExecutorIdAndTaskStatusReopened', {
                parent: 'entity',
                url: '/findAllByCurrentUserExecutorIdAndTaskStatusReopened',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserExecutorIdAndTaskStatusReopened.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllByCurrentUserExecutorIdAndTaskStatusOpened', {
                parent: 'entity',
                url: '/findAllByCurrentUserExecutorIdAndTaskStatusOpened',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllByCurrentUserExecutorIdAndTaskStatusOpened.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        //////////////
        $stateProvider
            .state('tasksWhichShouldStartToday', {
                parent: 'entity',
                url: '/tasksWhichShouldStartToday',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksWhichShouldStartToday.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksEstimated', {
                parent: 'entity',
                url: '/tasksEstimated',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksEstimated.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksOvertimed', {
                parent: 'entity',
                url: '/tasksOvertimed',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOvertimed.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksToEstimate', {
                parent: 'entity',
                url: '/tasksToEstimate',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksToEstimate.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksStatusesWithCount', {
                parent: 'entity',
                url: '/tasksStatusesWithCount',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksStatusesWithCount.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksPrioritiesWithCount', {
                parent: 'entity',
                url: '/tasksPrioritiesWithCount',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksPrioritiesWithCount.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('myTopAssigne', {
                parent: 'entity',
                url: '/myTopAssigne',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/myTopAssigne.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksWhichShouldEndToday', {
                parent: 'entity',
                url: '/tasksWhichShouldEndToday',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksWhichShouldEndToday.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal', {
                parent: 'entity',
                url: '/findAllTasksOfCurrentLoggedUserExecutorAndPriorityNormal',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUserExecutorWithPriorityNormal.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow', {
                parent: 'entity',
                url: '/findAllTasksOfCurrentLoggedUserExecutorAndPriorityLow',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUserExecutorWithPriorityLow.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh', {
                parent: 'entity',
                url: '/findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUserWithPriorityHigh.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal', {
                parent: 'entity',
                url: '/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUserWithPriorityNormal.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow', {
                parent: 'entity',
                url: '/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUserWithPriorityLow.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('tasksOfLoggedUser', {
                parent: 'entity',
                url: '/tasksOfLoggedUser',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/tasksOfLoggedUser.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('task', {
                parent: 'entity',
                url: '/task',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/task.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('task-executor', {
                parent: 'task',
                url: '/getCountOfTasksAssignedToUsersFromCurrentUserCompany',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/countOfTasks.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider
            .state('task-creator', {
                parent: 'task',
                url: '/getCountOfTasksCreatedByUsersFromCurrentUserCompany',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/countOfTasks.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
        $stateProvider.state('task-status', {
            parent: 'task',
            url: '/getTaskStatusesWithCount',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/task/countOfTasks.html',
                    controller: 'TaskController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('task');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
            .state('task-xml-generator', {
                parent: 'task',
                url: '/xmlGenerator',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/task.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('taskOfCurrentLoggedUser', {
                parent: 'task',
                url: '/allTasksOfCurrentLoggedUser',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/allTasksOfCurrentLoggedUser.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('creator-low', {
                parent: 'task',
                url: '/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllTasksOfCurrentLoggedUserCreatorAndPriorityLow.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('creator-high', {
                parent: 'task',
                url: '/findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllTasksOfCurrentLoggedUserCreatorAndPriorityHigh.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('creator-normal', {
                parent: 'task',
                url: '/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/findAllTasksOfCurrentLoggedUserCreatorAndPriorityNormal.html',
                        controller: 'TaskController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('task-detail', {
                parent: 'task',
                url: '/task/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jhipsterSampleApplicationApp.task.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/task/task-detail.html',
                        controller: 'TaskDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('task');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Task', function ($stateParams, Task) {
                        return Task.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'task',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('task-detail.edit', {
                parent: 'task-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/task/task-dialog.html',
                        controller: 'TaskDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Task', function (Task) {
                                return Task.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('task.new', {
                parent: 'task',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/task/task-dialog.html',
                        controller: 'TaskDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    balance: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('task', null, {reload: 'task'});
                    }, function () {
                        $state.go('task');
                    });
                }]
            })
            .state('task.edit', {
                parent: 'task',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/task/task-dialog.html',
                        controller: 'TaskDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Task', function (Task) {
                                return Task.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('task', null, {reload: 'task'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('task.delete', {
                parent: 'task',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/task/task-delete-dialog.html',
                        controller: 'TaskDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Task', function (Task) {
                                return Task.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('task', null, {reload: 'task'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
