(function() {
    'use strict';

    angular
        .module('jhipsterSampleApplicationApp')
        .controller('FeedbackController', FeedbackController);

    FeedbackController.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$rootScope', '$modal', 'Handler', '$stateParams', '$translate', 'Notify',  'Auth', '$upload', '$resource', 'FeedbackService'];

    function FeedbackController ($scope, Principal, LoginService, $state, $rootScope, $modal, Handler, $stateParams, $translate, Notify,  Auth, $upload, $resource, FeedbackService) {
        var vm = this;
        //
        // vm.account = null;
        // vm.isAuthenticated = null;
        // vm.login = LoginService.open;
        // vm.register = register;
        // $scope.$on('authenticationSuccess', function() {
        //     getAccount();
        // });
        //
        // getAccount();
        //
        // function getAccount() {
        //     Principal.identity().then(function(account) {
        //         vm.account = account;
        //         vm.isAuthenticated = Principal.isAuthenticated;
        //     });
        // }
        // function register () {
        //     $state.go('register');
        // }
        $scope.maxlengthtest = function(){
            return document.getElementById('feedbackInput').value.length;
        }
        $scope.send = function(feedbackText){
            $scope.feedbackText = feedbackText;
            FeedbackService.postFeedback($scope.feedbackText).then(function(data){
                Notify.addNotify('success', $translate.instant('global.notify.feedbacksent'));
                $scope.close();
            }, Handler.error);
        };

        $scope.close = function () {
            $scope.$close(true);
        };
    }
})();


'use strict';

angular.module('jhipsterSampleApplicationApp')
    .controller('FeedbackController', function ($scope, $rootScope, $modal, Handler, $stateParams, $translate, Notify, Principal, Auth, $upload, $resource, FeedbackService) {

    	$scope.maxlengthtest = function(){
    		return document.getElementById('feedbackInput').value.length;
    	}
        $scope.send = function(feedbackText){
            $scope.feedbackText = feedbackText;
            FeedbackService.postFeedback($scope.feedbackText).then(function(data){
                Notify.addNotify('success', $translate.instant('global.notify.feedbacksent'));
                $scope.close();
            }, Handler.error);
        };

        $scope.close = function () {
            $scope.$close(true);
    };

    });
