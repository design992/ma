'use strict';

angular.module('jhipsterSampleApplicationApp')
     .factory('FeedbackService', function ($http,Base64) {
        return {
            postFeedback: function(feedbackText) {
                return $http.post('api/feedback/sendFeedback', feedbackText, {

                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Basic " + Base64.encode("jhipsterSampleApplicationApp" + ':' + "mySecretOAuthSecret")
                    }
                }).then(function (response) {
                    return response.data;
                });
            }
        }
    });
