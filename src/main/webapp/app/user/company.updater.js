'use strict';

angular.module('jhipsterSampleApplicationApp')
	.service('CompanyInfoUpdater', ['CompanyService', function (CompanyService) {

    var onUpdateCallbacks = {};
    var fireUpdateCallbacks = function () {
    	CompanyService.getCompanyInfo().then(function(companyAccount) {
    		for (var i in onUpdateCallbacks) {
                onUpdateCallbacks[i](companyAccount);
            }
    	});
    };

    this.fireUpdateCallbacks = fireUpdateCallbacks;
    /**
     * Dodaje funkcję do funkcji wywoływanych podczas aktualizacji
     */
    this.addUpdateCallback = function (name, callback) {
        onUpdateCallbacks[name]=callback;
    };

    this.removeUpdateCallback = function (name) {
        delete onUpdateCallbacks[name];
    };

    this.update = function () {
    	fireUpdateCallbacks();
    };

}]);
