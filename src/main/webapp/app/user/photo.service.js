'use strict';

angular.module('jhipsterSampleApplicationApp')
    .factory('PhotoService', function ($http,$upload,Base64) {
        return {
        	postUserPhoto : function (file) {
            	return $upload.upload({
                    url: 'photo/upload',
                    file: file,
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "Authorization": "Basic " + Base64.encode("agileToolBoxapp" + ':' + "mySecretOAuthSecret")
                    }
                });

            },
            deleteUserPhotos : function () {
                return $http.get('api/user/photo/delete').then(function (response) {
                    return response.data;
                });
            }
        };
    });

