'use strict';

angular.module('jhipsterSampleApplicationApp')
	.service('UserUpdater', ['Principal', function (Principal) {

    var onUpdateCallbacks = {};
    var fireUpdateCallbacks = function () {
    	Principal.identity(true).then(function(account) {
    		for (var i in onUpdateCallbacks) {
                onUpdateCallbacks[i](account);
            }
    	});
    };

    this.fireUpdateCallbacks = fireUpdateCallbacks;
    /**
     * Dodaje funkcję do funkcji wywoływanych podczas aktualizacji
     */
    this.addUpdateCallback = function (name, callback) {
        onUpdateCallbacks[name]=callback;
    };

    this.removeUpdateCallback = function (name) {
        delete onUpdateCallbacks[name];
    };

    this.update = function () {
    	fireUpdateCallbacks();
    };

}]);
